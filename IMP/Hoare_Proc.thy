(* Author: Peter Lammich *)
chapter \<open>WP Style VCG with Mutually Recursive Procedures and Variables\<close>
theory Hoare_Proc
imports 
  Main
  "~~/src/HOL/Eisbach/Eisbach"
begin

text \<open>
  This illustrates a verification infrastructure for a small imperative toy language with
  mutually recursive procedures and local and global variables. 
  Moreover, the language is modular in the sense that we can import already verified
  programs as modules.
  
  The supported tooling is kept minimal for didactic purposes, to not 
  obfuscate the underlying concepts too much.
  
  We derive a simple verification condition generator, and conclude with the verification of 
  some examples, including a collection of contrived examples from Peter V. Homeier's phd thesis.
\<close>


section "Expressions"

type_synonym vname = string
type_synonym val = int
  
datatype aexp = 
    N int            -- \<open>Constant\<close>
  | VL vname         -- \<open>Local Variable\<close> 
  | VG vname         -- \<open>Global Variable\<close> 
  | Binop "int \<Rightarrow> int \<Rightarrow> int" aexp aexp -- \<open>Binary operation\<close>


abbreviation "Plus \<equiv> Binop (op+)"
abbreviation "Minus \<equiv> Binop (op-)"
abbreviation "Times \<equiv> Binop (op*)"

text \<open>Warning, the following two also give defined results on division by zero!\<close>
value "a div (0::int)" value "a mod (0::int)"
abbreviation "Div \<equiv> Binop (op div)"
abbreviation "Mod \<equiv> Binop (op mod)"

subsection \<open>Boolean Expressions\<close>
datatype bexp = 
    Bc bool    -- \<open>True or False\<close>
  | Not bexp   -- \<open>Negation\<close>
  | BBop "bool\<Rightarrow>bool\<Rightarrow>bool" bexp bexp   -- \<open>Binary boolean operation\<close>
  | Cmpop "int \<Rightarrow> int \<Rightarrow> bool" aexp aexp -- \<open>Comparison operation\<close>

abbreviation "And \<equiv> BBop op \<and>"
abbreviation "Or \<equiv> BBop op \<or>"
abbreviation "BNEq \<equiv> BBop op \<noteq>"
abbreviation "BEq \<equiv> BBop op \<longleftrightarrow>"

abbreviation "Less \<equiv> Cmpop op <"
abbreviation "Leq \<equiv> Cmpop op \<le>"
abbreviation "Greater \<equiv> Cmpop op >"
abbreviation "Geq \<equiv> Cmpop op \<ge>"
abbreviation "Equal \<equiv> Cmpop op ="
abbreviation "NEqual \<equiv> Cmpop op \<noteq>"


section \<open>Commands\<close>
type_synonym pname = string

datatype
  com = SKIP 
      | AssignL vname aexp      ("_ ::= _" [900, 61] 61)
      | AssignG vname aexp      ("_ G::= _" [900, 61] 61)
      | Seq    com  com         ("_;;/ _"  [60, 61] 60)
      | If     bexp com com     ("(IF _/ THEN _/ ELSE _)"  [0, 0, 61] 61)
      | While  bexp com         ("(WHILE _/ DO _)"  [0, 61] 61)
      | Call   pname            
      | Scope  com              


text \<open>It makes sense to always scope a call, such that locals gets properly 
  initialized and restored\<close>
definition SCall ("SCALL _" [61] 61) where "SCall p \<equiv> Scope (Call p)"

section \<open>Example Program\<close>
(*
  fib (n) {
    if n<2 then
      return 1
    else {
      r = fib (n-1)
      return r + fib (n-2)
    }
  }
*)


definition "fib \<equiv> 
  ''n'' ::= VG ''par1'';;
  IF (Less (VL ''n'') (N 2)) THEN
    ''R'' G::= N 1
  ELSE (
    (* r = fib (n-1)  *)
    ''par1'' G::= Minus (VL ''n'') (N 1);;
    SCALL ''fib'';;
    ''r'' ::= VG ''R'';;
    
    (* R = r + fib (n-2) *)
    ''par1'' G::= Minus (VL ''n'') (N 2);;
    SCALL ''fib'';;
    ''R'' G::= Plus (VL ''r'') (VG ''R'')
)"

      
section \<open>Semantics\<close>

text \<open>The state consists of two separate valuations, one for locals and one for globals\<close>
type_synonym valuation = "vname \<Rightarrow> val"
type_synonym state = "valuation \<times> valuation"

definition null_state :: "vname \<Rightarrow> val" ("<>") where
  "null_state \<equiv> \<lambda>x. 0"
syntax 
  "_State" :: "updbinds => 'a" ("<_>")
translations
  "_State ms" == "_Update <> ms"
  "_State (_updbinds b bs)" <= "_Update (_State b) bs"

text {* \noindent
  We can now write a series of updates to the function @{text "\<lambda>x. 0"} compactly:
*}
lemma "<a := 1, b := 2> = (<> (a := 1)) (b := (2::int))"
  by (rule refl)


fun aval :: "aexp \<Rightarrow> state \<Rightarrow> val" where
"aval (N n) s = n" |
"aval (VL x) (l,g) = l x" |
"aval (VG x) (l,g) = g x" |
"aval (Binop f a1 a2) s = f (aval a1 s) (aval a2 s)"

fun bval :: "bexp \<Rightarrow> state \<Rightarrow> bool" where
"bval (Bc v) s \<longleftrightarrow> v" |
"bval (Not b) s \<longleftrightarrow> \<not>(bval b s)" |
"bval (BBop f b1 b2) s \<longleftrightarrow> f (bval b1 s) (bval b2 s)" |
"bval (Cmpop f a1 a2) s \<longleftrightarrow> f (aval a1 s) (aval a2 s)"


value "aval (Plus (VL ''x'') (N 5)) (<''x'' := 7>,<>)"
value "<>"  


text \<open>Semantics is characterized over a procedure environment, 
  that maps procedure names to commands ... or @{term None}, if procedure name is not defined.\<close>
type_synonym penv = "pname \<Rightarrow> com option"

inductive
  big_step :: "penv \<Rightarrow> com \<times> state \<Rightarrow> state \<Rightarrow> bool" ("_: _ \<Rightarrow> _" [55,55,55] 55)
  for pe
where
Skip: "pe: (SKIP,s) \<Rightarrow> s" |
AssignL: "pe: (x ::= a,l,g) \<Rightarrow> (l(x := aval a (l,g)),g)" |
AssignG: "pe: (x G::= a,l,g) \<Rightarrow> (l,g(x := aval a (l,g)))" |
Seq: "\<lbrakk> pe: (c\<^sub>1,s\<^sub>1) \<Rightarrow> s\<^sub>2;  pe: (c\<^sub>2,s\<^sub>2) \<Rightarrow> s\<^sub>3 \<rbrakk> \<Longrightarrow> pe: (c\<^sub>1;;c\<^sub>2, s\<^sub>1) \<Rightarrow> s\<^sub>3" |
IfTrue: "\<lbrakk> bval b s;  pe: (c\<^sub>1,s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> pe: (IF b THEN c\<^sub>1 ELSE c\<^sub>2, s) \<Rightarrow> t" |
IfFalse: "\<lbrakk> \<not>bval b s;  pe: (c\<^sub>2,s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> pe: (IF b THEN c\<^sub>1 ELSE c\<^sub>2, s) \<Rightarrow> t" |
WhileFalse: "\<not>bval b s \<Longrightarrow> pe: (WHILE b DO c,s) \<Rightarrow> s" |
WhileTrue:
"\<lbrakk> bval b s\<^sub>1;  pe: (c,s\<^sub>1) \<Rightarrow> s\<^sub>2;  pe: (WHILE b DO c, s\<^sub>2) \<Rightarrow> s\<^sub>3 \<rbrakk> 
  \<Longrightarrow> pe: (WHILE b DO c, s\<^sub>1) \<Rightarrow> s\<^sub>3" |
Call: "\<lbrakk> pe p = Some c; pe: (c,s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> pe: (Call p,s) \<Rightarrow> t" |
Scope: "\<lbrakk> pe: (c,<>,g) \<Rightarrow> (_,g') \<rbrakk> \<Longrightarrow> pe: (Scope c,l,g) \<Rightarrow> (l,g')"
  
  
text{* Proof automation: *}
declare big_step.intros [intro]
lemmas big_step_induct = big_step.induct[split_format(complete)]
thm big_step_induct
inductive_cases SkipE[elim!]: "pe: (SKIP,s) \<Rightarrow> t"
thm SkipE

inductive_cases AssignE[elim!]: 
  "pe: (x ::= a,s) \<Rightarrow> t"
  "pe: (x G::= a,s) \<Rightarrow> t"
thm AssignE
inductive_cases SeqE[elim!]: "pe: (c1;;c2,s1) \<Rightarrow> s3"
thm SeqE
inductive_cases IfE[elim!]: "pe: (IF b THEN c1 ELSE c2,s) \<Rightarrow> t"
thm IfE

inductive_cases WhileE[elim]: "pe: (WHILE b DO c,s) \<Rightarrow> t"
thm WhileE
text{* Only [elim]: [elim!] would not terminate. *}

inductive_cases CallE[elim!]: "pe: (Call p,s) \<Rightarrow> t"
thm CallE
inductive_cases ScopeE[elim!]: "pe: (Scope c,s) \<Rightarrow> t"
thm ScopeE


text {* An example combining rule inversion and derivations *}
lemma Seq_assoc:
  "pe: ((c1;; c2);; c3, s) \<Rightarrow> s' \<longleftrightarrow> pe: (c1;; (c2;; c3), s) \<Rightarrow> s'"
proof
  assume "pe: (c1;; c2;; c3, s) \<Rightarrow> s'"
  then obtain s1 s2 where
    c1: "pe: (c1, s) \<Rightarrow> s1" and
    c2: "pe: (c2, s1) \<Rightarrow> s2" and
    c3: "pe: (c3, s2) \<Rightarrow> s'" by auto
  from c2 c3
  have "pe: (c2;; c3, s1) \<Rightarrow> s'" by (rule Seq)
  with c1
  show "pe: (c1;; (c2;; c3), s) \<Rightarrow> s'" by (rule Seq)
next
  -- "The other direction is analogous"
  assume "pe: (c1;; (c2;; c3), s) \<Rightarrow> s'"
  thus "pe: (c1;; c2;; c3, s) \<Rightarrow> s'" by auto
qed



subsubsection "Execution is deterministic"

theorem big_step_determ: "\<lbrakk> pe: (c,s) \<Rightarrow> t; pe: (c,s) \<Rightarrow> u \<rbrakk> \<Longrightarrow> u = t"
proof (induction arbitrary: u rule: big_step.induct)
  case (Call p c s t)
  then show ?case 
    by (erule_tac CallE) simp
qed blast+
  
section \<open>Weakest Precondition\<close>  
  
definition "wp pe c Q \<equiv> \<lambda>s. \<exists>t. pe: (c,s) \<Rightarrow> t \<and> Q t"

context 
  notes [simp] = wp_def
begin
  lemma wp_mono: "\<lbrakk>\<And>s. P s \<Longrightarrow> Q s; wp pe c P s\<rbrakk> \<Longrightarrow> wp pe c Q s" by auto

  lemma wp_skip: "wp pe SKIP Q s = Q s" by (cases s) auto
  lemma wp_assign: 
    "wp pe (x::=a) Q (l,g) \<longleftrightarrow> Q (l(x:=aval a (l,g)),g)" 
    "wp pe (x G::= a) Q (l,g) \<longleftrightarrow> Q (l,g(x:=aval a (l,g)))" 
    by auto
  lemma wp_seq: "wp pe (c1;;c2) Q s \<longleftrightarrow> wp pe c1 (\<lambda>t. wp pe c2 Q t) s" by auto
  lemma wp_if: "wp pe (IF b THEN c1 ELSE c2) Q s \<longleftrightarrow> (if bval b s then wp pe c1 Q s else wp pe c2 Q s)" by auto
  lemma wp_while: "wp pe (WHILE b DO c) Q s \<longleftrightarrow> wp pe (IF b THEN c;;WHILE b DO c ELSE SKIP) Q s" by force
  lemma wp_call: "wp pe (Call p) Q s \<longleftrightarrow> (\<exists>c. pe p = Some c \<and> wp pe c Q s)" by auto
  lemma wp_scope: "wp pe (Scope c) Q (l,g) \<longleftrightarrow> wp pe c (\<lambda>(_,g'). Q (l,g')) (<>,g)"
    by auto
  
end  

lemma wp_scall: "wp pe (SCALL p) Q (l,g) \<longleftrightarrow> (\<exists>c. pe p = Some c \<and> wp pe c (\<lambda>(_,g'). Q (l,g')) (<>,g))"
  unfolding SCall_def by (simp add: wp_call wp_scope)
    
lemmas wp_simps = wp_skip wp_assign wp_seq wp_if wp_call wp_scope wp_scall
  
  

context 
  notes [simp] = wp_simps
begin
lemma wpI_skip: "Q s \<Longrightarrow> wp pe SKIP Q s" by auto
lemma wpI_assign: 
  "Q (l(x:=aval a (l,g)),g) \<Longrightarrow> wp pe (x ::= a) Q (l,g)" 
  "Q (l,g(x:=aval a (l,g))) \<Longrightarrow> wp pe (x G::= a) Q (l,g)" 
  by auto
lemma wpI_seq: "wp pe c1 (wp pe c2 Q) s \<Longrightarrow> wp pe (c1;;c2) Q s" by auto
lemma wpI_if: "\<lbrakk>bval b s \<Longrightarrow> wp pe c1 Q s; \<not>bval b s \<Longrightarrow> wp pe c2 Q s\<rbrakk> \<Longrightarrow> wp pe (IF b THEN c1 ELSE c2) Q s" by auto
lemma wpI_scope: "wp pe c (\<lambda>(_,g'). Q (l,g')) (<>,g) \<Longrightarrow> wp pe (Scope c) Q (l,g)" by auto

text \<open>Work for non-recursive calls\<close>
lemma wpI_call: "\<lbrakk>pe p = Some c; wp pe c Q s\<rbrakk> \<Longrightarrow> wp pe (Call p) Q s" by auto
  
lemma wpI_scall: "\<lbrakk>
    pe p = Some c; 
    wp pe c (\<lambda>(_,g'). Q (l,g')) (<>,g)
  \<rbrakk> \<Longrightarrow> wp pe (SCALL p) Q (l,g)"
  by auto

lemmas wpI_rules = wpI_skip wpI_assign wpI_seq wpI_if wpI_scope 
  
end

section \<open>Reasoning about Local Variables\<close>  
context 
  notes [simp] = wp_simps
begin

text \<open>
  We will use calls always in combination with scoping.
  Thus, the specification of a procedure call needs to only mention global variables.
  (The locals will be cleared and restored anyway).
  
  We make this explicit, by defining Hoare-triples for procedures:
\<close>

type_synonym precond = "valuation \<Rightarrow> bool"
type_synonym postcond = "valuation \<Rightarrow> valuation \<Rightarrow> bool"
type_synonym ptriple = "precond \<times> pname \<times> postcond"

definition spec :: "penv \<Rightarrow> ptriple \<Rightarrow> bool" where
  "spec pe \<equiv> \<lambda>(P,p,Q). (\<forall>l g. P g \<longrightarrow> wp pe (SCALL p) (\<lambda>(_,g'). Q g g') (l,g))"

text \<open>Alternatively, we can define the triple over the procedure body.
  The latter characterization is suitable for proving specifications,
  while the former is suitable for using them.
\<close>
lemma spec_alt: "spec pe = (\<lambda>(P,p,Q). 
  (\<forall>g. P g \<longrightarrow> (\<exists>c. pe p = Some c \<and> (wp pe c (\<lambda>(_,g'). Q g g') (<>,g)))))"
  unfolding spec_def by auto

text \<open>Using a specification\<close>  
lemma wpI_callspec:
  assumes "spec pe (P,p,Q)"
  assumes "P g"
  assumes "\<And>g'. Q g g' \<Longrightarrow> R (l,g')"
  shows "wp pe (SCALL p) R (l,g)"
  using assms
  apply (clarsimp simp: spec_alt)
  unfolding wp_def by fastforce

text \<open>Proving a specification (non-recursive)\<close>  
lemma specI_nonrec:
  assumes "pe p = Some c"
  assumes "\<And>g. P g \<Longrightarrow> wp pe c (\<lambda>(_,g'). Q g g') (<>,g)"
  shows "spec pe (P,p,Q)" 
  using assms unfolding spec_def by auto

end

section \<open>Reasoning about Recursion\<close>

context 
  notes [simp] = wp_simps
begin

text \<open>For WHILE-Loops, we have to come up with a variant and an invariant\<close>
lemma wpI_while_basic:
  assumes WF: "wf R"
  assumes I0: "I s\<^sub>0"
  assumes IS: "\<And>s\<^sub>1. \<lbrakk>I s\<^sub>1; bval b s\<^sub>1 \<rbrakk> \<Longrightarrow> wp pe c (\<lambda>s\<^sub>2. I s\<^sub>2 \<and> (s\<^sub>2,s\<^sub>1)\<in>R) s\<^sub>1"  
  assumes IF: "\<And>s s. \<lbrakk>I s; \<not>bval b s \<rbrakk> \<Longrightarrow> Q s"
  shows "wp pe (While b c) Q s\<^sub>0"
  using assms(1,2)
  apply (induction s\<^sub>0 rule: wf_induct_rule)
  apply (subst wp_while; simp)
  using IS by (fastforce simp: wp_def intro: IF)

lemma wpI_while:
  assumes WF: "wf R"
  assumes I0: "I s\<^sub>0 s\<^sub>0"
  assumes IS: "\<And>s\<^sub>1. \<lbrakk>I s\<^sub>0 s\<^sub>1; bval b s\<^sub>1 \<rbrakk> \<Longrightarrow> wp pe c (\<lambda>s\<^sub>2. I s\<^sub>0 s\<^sub>2 \<and> (s\<^sub>2,s\<^sub>1)\<in>R) s\<^sub>1"  
  assumes IF: "\<And>s s. \<lbrakk>I s\<^sub>0 s; \<not>bval b s \<rbrakk> \<Longrightarrow> Q s"
  shows "wp pe (While b c) Q s\<^sub>0"
  using assms by (rule wpI_while_basic)
  
text \<open>For calls, we show that a call satisfies its specification, 
  assuming that it already satisfies its specification for 
  smaller states (wrt. a well-founded relation).
\<close>

text \<open>Proving that a call satisfies the specification P,Q: \<close>
lemma wpI_proc_prove_basic_first_attempt: 
  assumes "wf R"
  assumes RL: "\<And>s. \<lbrakk> 
    P s;
    (\<And>t. \<lbrakk>P t; (t,s)\<in>R \<rbrakk> \<Longrightarrow> wp pe (Call p) Q t)
  \<rbrakk> \<Longrightarrow> wp pe (Call p) Q s"
  assumes "P s"
  shows "wp pe (Call p) Q s"
  using assms(1,3)
  apply (induction s rule: wf_induct_rule)
  using RL by blast

text \<open>We might want to pass logical variables from the precondition to the postcondition.
  We always pass the old state to the postcondition.
  Example: @{term "P s\<^sub>0 \<longleftrightarrow> s\<^sub>0 ''x'' > 0"} and @{term "Q s\<^sub>0 s \<longleftrightarrow> s ''r'' = (s\<^sub>0 ''x'')\<^sup>2"}
\<close>  
lemma wpI_proc_prove_basic: 
  assumes "wf R"
  assumes RL: "\<And>s. \<lbrakk> 
    P s;
    (\<And>t. \<lbrakk>P t; (t,s)\<in>R \<rbrakk> \<Longrightarrow> wp pe (Call p) (\<lambda>t'. Q t t') t)
  \<rbrakk> \<Longrightarrow> wp pe (Call p) (Q s) s"
  assumes "P s"
  shows "wp pe (Call p) (\<lambda>s'. Q s s') s"
  using assms(1,3)
  apply (induction s rule: wf_induct_rule)
  using RL by blast

text \<open>Procedures may be mutually recursive. 
  In this case, we must simultaneously show a specification for all involved procedures.
\<close>
lemma wpI_proc_prove_basic_mutually_recursive: 
  assumes "wf R"
  assumes RL: "\<And>s P p Q. \<lbrakk> 
    (P,p,Q) \<in> \<Theta>; P s;
    (\<And>t P' p' Q'. \<lbrakk>(P',p',Q')\<in>\<Theta>; P' t; ((t,p'),(s,p))\<in>R \<rbrakk> \<Longrightarrow> wp pe (Call p') (Q' t) t)
  \<rbrakk> \<Longrightarrow> wp pe (Call p) (Q s) s"
  shows "\<forall>(P,p,Q) \<in> \<Theta>. \<forall>s. P s \<longrightarrow> wp pe (Call p) (Q s) s"
  proof (intro ballI allI impI, (split prod.splits)+, intro allI impI, hypsubst)
    fix P p Q s
    assume "(P,p,Q) \<in> \<Theta>" "P s"
    with \<open>wf R\<close> show "wp pe (Call p) (Q s) s" 
      apply (induction "(s,p)" arbitrary: s p P Q rule: wf_induct_rule)
      using RL by blast
  qed


text \<open>Proving a specification (simply recursive)\<close>  
lemma spec1I:
  assumes WF: "wf R"
  assumes DEF: "pe p = Some c"
  assumes RL: "\<And>g\<^sub>0. \<lbrakk> P g\<^sub>0;
    (spec pe (\<lambda>g. P g \<and> (g,g\<^sub>0)\<in>R, p, Q))
    \<rbrakk> \<Longrightarrow> wp pe c (\<lambda>(_,g'). Q g\<^sub>0 g') (<>,g\<^sub>0)"
  shows "spec pe (P,p,Q)"
proof -  
  {
    fix g
    assume "P g"
    with WF have "wp pe c (\<lambda>(_,g'). Q g g') (<>,g)"
    proof (induction g rule: wf_induct_rule)
      case (less g)
      show ?case
        apply (rule RL)
        apply fact
        using less.IH DEF unfolding spec_alt by auto
    qed
  }
  with DEF show ?thesis unfolding spec_alt by auto
qed  
  
text \<open>Proving a specification (mutually recursive)\<close>  
lemma specI_basic:
  assumes WF: "wf R"
  assumes DEF: "\<And>P p Q. (P,p,Q)\<in>\<Theta> \<Longrightarrow> pe p \<noteq> None"
  assumes RL: "\<And>g\<^sub>0 c P p Q. \<lbrakk>
    (P,p,Q)\<in>\<Theta>;
    pe p = Some c;
    P g\<^sub>0;
    (\<And>P\<^sub>r p\<^sub>r Q\<^sub>r. (P\<^sub>r, p\<^sub>r, Q\<^sub>r)\<in>\<Theta> \<Longrightarrow> spec pe (\<lambda>g. P\<^sub>r g \<and> ((g,p\<^sub>r),(g\<^sub>0,p))\<in>R, p\<^sub>r, Q\<^sub>r))
    \<rbrakk> \<Longrightarrow> wp pe c (\<lambda>(_,g'). Q g\<^sub>0 g') (<>,g\<^sub>0)"
  shows "\<forall>\<theta>\<in>\<Theta>. spec pe \<theta>"
proof -  
  {
    fix P p Q c g
    assume A: "(P,p,Q)\<in>\<Theta>" "P g" and D: "pe p = Some c"
    have "wp pe c (\<lambda>(_,g'). Q g g') (<>,g)" 
      using WF A D
      proof (induction "(g,p)" arbitrary: g p P Q c)
        case less
        show ?case
          apply (rule RL)
          apply fact
          apply fact
          apply fact
          using less.hyps DEF 
          unfolding spec_alt by blast
      qed
  }
  with DEF show ?thesis
    unfolding spec_alt by blast
qed  
  
type_synonym recstate = "valuation \<times> pname"
type_synonym recrel = "(recstate \<times> recstate) set"

definition "SPECS pe \<Theta> \<equiv> \<forall>\<theta>\<in>\<Theta>. spec pe \<theta>"  
definition RECSPECS 
  :: "recstate \<Rightarrow> recrel \<Rightarrow> penv \<Rightarrow> ptriple set \<Rightarrow> bool"
  where "RECSPECS gp\<^sub>0 R pe \<Theta> 
  \<equiv> \<forall>(P,p,Q)\<in>\<Theta>. spec pe (\<lambda>g. P g \<and> ((g,p),gp\<^sub>0)\<in>R, p, Q)"

lemma specI:
  assumes WF: "wf R"
  assumes DEF: "\<And>P p Q. (P,p,Q)\<in>\<Theta> \<Longrightarrow> pe p \<noteq> None"
  assumes RL: "\<And>g\<^sub>0 c P p Q. \<lbrakk>
    (P,p,Q)\<in>\<Theta>;
    pe p = Some c;
    P g\<^sub>0;
    RECSPECS (g\<^sub>0,p) R pe \<Theta>
    \<rbrakk> \<Longrightarrow> wp pe c (\<lambda>(_,g'). Q g\<^sub>0 g') (<>,g\<^sub>0)"
  shows "SPECS pe \<Theta>"
  unfolding SPECS_def
  apply (rule specI_basic[OF WF])
  apply fact
  apply (rule RL, assumption+)
  unfolding RECSPECS_def by blast
  
lemma use_SPECSI:
  assumes "SPECS pe \<Theta>"
  assumes "(P,p,Q)\<in>\<Theta>"
  assumes "P g"
  assumes "\<And>g'. Q g g' \<Longrightarrow> R (l,g')"
  shows "wp pe (SCALL p) R (l,g)"  
  using assms unfolding SPECS_def by (blast intro: wpI_callspec)
  
lemma use_RECSPECSI:
  assumes "RECSPECS gp\<^sub>0 Rel pe \<Theta>"
  assumes "(P,p,Q)\<in>\<Theta>"
  assumes "((g,p),gp\<^sub>0)\<in>Rel"
  assumes "P g"
  assumes "\<And>g'. Q g g' \<Longrightarrow> R (l,g')"
  shows "wp pe (SCALL p) R (l,g)"  
  using assms unfolding RECSPECS_def by (blast intro: wpI_callspec)


end


section \<open>Modularity\<close>

text \<open>We show that we can link together programs, as long as their procedure names
       do not clash\<close>
       
lemma map_mp: "m1 \<subseteq>\<^sub>m m2 \<Longrightarrow> m1 k = Some v \<Longrightarrow> m2 k = Some v"
  unfolding map_le_def by (auto simp: dom_def)
       
lemma map_rev_mp: "m1 k = Some v \<Longrightarrow> m1 \<subseteq>\<^sub>m m2 \<Longrightarrow> m2 k = Some v"
  using map_mp .
       
lemma lift_execution:
  assumes "pe\<^sub>1 \<subseteq>\<^sub>m pe\<^sub>2"
  assumes "pe\<^sub>1: (c,s) \<Rightarrow> t"
  shows "pe\<^sub>2: (c,s) \<Rightarrow> t"
  using assms(2)
  apply (induction)
  using assms(1)
  by (auto intro: map_mp)
  
       
lemma lift_spec:
  assumes "pe\<^sub>1 \<subseteq>\<^sub>m pe\<^sub>2"
  assumes "spec pe\<^sub>1 \<theta>"
  shows "spec pe\<^sub>2 \<theta>"
  using assms(2)
  unfolding spec_def wp_def
  apply (cases \<theta>)
  apply (fastforce intro: lift_execution[OF assms(1)])
  done

lemma lift_SPECS:  
  assumes "pe\<^sub>1 \<subseteq>\<^sub>m pe\<^sub>2"
  assumes "SPECS pe\<^sub>1 \<Theta>"
  shows "SPECS pe\<^sub>2 \<Theta>"
  using assms(2) lift_spec[OF assms(1)]
  unfolding SPECS_def by blast
  

(* Missing lemmas on map_le *)    
lemma map_le_upd[simp]: "m(k\<mapsto>v) \<subseteq>\<^sub>m m' \<longleftrightarrow> m' k = Some v \<and> m(k:=None) \<subseteq>\<^sub>m m'"  
  by (auto simp: map_le_def)

lemma map_upd_subst[simp]: "k\<noteq>k' \<Longrightarrow> (m(k\<mapsto>v))(k':=None) = (m(k':=None))(k\<mapsto>v)" by auto
  
  
subsection \<open>Modified Variables\<close>  

definition eq_on :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> 'a set \<Rightarrow> bool" ("_ = _ on _" [50,50,50] 50)
  where "s=t on X \<longleftrightarrow> (\<forall>x\<in>X. s x = t x)"
  
lemma eq_on_subst_same[simp]: 
  "x\<in>X \<Longrightarrow> s(x:=v) = t on X \<longleftrightarrow> t x = v \<and> s=t on (X-{x})"  
  "x\<in>X \<Longrightarrow> s = t(x:=v) on X \<longleftrightarrow> s x = v \<and> s=t on (X-{x})"  
  by (auto simp: eq_on_def)
  
lemma eq_on_subst_other[simp]: 
  "x\<notin>X \<Longrightarrow> s(x:=v) = t on X \<longleftrightarrow> s=t on X"
  "x\<notin>X \<Longrightarrow> s = t(x:=v) on X \<longleftrightarrow> s=t on X"
  by (auto simp: eq_on_def)
  
lemma eq_on_refl[simp]: "s = s on X"  
  by (auto simp: eq_on_def)
  


subsection \<open>Ad-hoc macros for calling convention\<close>
(* We can set up a generic macro infrastructure to handle our calling conventions.
  Here, we only illustrate the principle.
*)
abbreviation "call1 x p a1 \<equiv> 
  ''par1'' G::= a1;;
  SCALL p;;
  x ::= VG ''R''
"

abbreviation "proc1 x1 c \<equiv> 
  x1 ::= VG ''par1'';;
  c
"

abbreviation "vcall1 p a1 \<equiv> 
  ''par1'' G::= a1;;
  SCALL p
"

abbreviation "vcall2 p a1 a2 \<equiv> 
  ''par1'' G::= a1;;
  ''par2'' G::= a2;;
  SCALL p
"

abbreviation "call2 x p a1 a2 \<equiv> 
  ''par1'' G::= a1;;
  ''par2'' G::= a2;;
  SCALL p;;
  x ::= VG ''R'' 
"


abbreviation "proc2 x1 x2 c \<equiv> 
  x1 ::= VG ''par1'';;
  x2 ::= VG ''par2'';;
  c
"

  
section \<open>Prototype VCG\<close>  
method vcg_step = (
      intro wpI_rules; simp?
    | (clarsimp; elim disjE; clarsimp?)  
    | (rule use_RECSPECSI, assumption, (simp;fail))
    | (rule use_SPECSI, assumption, (simp;fail))
    | (rule wpI_callspec, assumption)
    | ((auto) [])
  )
method vcg = vcg_step+
  

section \<open>Examples\<close>

subsection \<open>Manual Proofs\<close>

subsubsection \<open>Recursive Fibonacci\<close>

function ifib :: "int \<Rightarrow> int" where
  "i<2 \<Longrightarrow> ifib i = 1"
| "i\<ge>2 \<Longrightarrow> ifib i = ifib (i-1) + ifib (i-2)"
  apply auto by fastforce
termination by lexicographic_order
  
thm ifib.simps

definition "fib_pe \<equiv> [''fib''\<mapsto>fib]"
lemma [simp]: "fib_pe ''fib'' = Some fib" by (auto simp: fib_pe_def)


lemma "spec fib_pe (\<lambda>_. True, ''fib'', \<lambda>g\<^sub>0 g. g ''R'' = ifib (g\<^sub>0 ''par1''))"
  apply (rule spec1I[where R="measure (\<lambda>g. nat (g ''par1''))"])
  apply simp
  apply (simp)
  apply simp (* Unfolding measure \<dots>*)
  unfolding fib_def
  apply (intro wpI_rules)
  apply simp
  apply (rule wpI_callspec, assumption)
  apply simp
  apply (intro wpI_rules)
  apply (rule wpI_callspec, assumption)
  apply simp
  apply (intro wpI_rules)
  apply (simp)
  done

  
subsubsection \<open>Mutually Recursive Odd-Even\<close>
    
definition "odd_body \<equiv> 
  ''n'' ::= VG ''par1'';;
  IF Equal (VL ''n'') (N 0) THEN
    ''R'' G::= N 0
  ELSE IF Less (VL ''n'') (N 0) THEN (
    ''par1'' G::= Plus (VL ''n'') (N 1);;
    SCALL ''even''
  ) ELSE (
    ''par1'' G::= Minus (VL ''n'') (N 1);;
    SCALL ''even''
  )"
  
definition "even_body \<equiv> 
  ''n'' ::= VG ''par1'';;
  IF Equal (VL ''n'') (N 0) THEN
    ''R'' G::= N 1
  ELSE IF Less (VL ''n'') (N 0) THEN (
    ''par1'' G::= Plus (VL ''n'') (N 1);;
    SCALL ''odd''
  ) ELSE (
    ''par1'' G::= Minus (VL ''n'') (N 1);;
    SCALL ''odd''
  )"
  
definition "odd_even_pe \<equiv> [''odd'' \<mapsto> odd_body, ''even'' \<mapsto> even_body]"
  
lemma [simp]:
  "odd_even_pe ''odd'' = Some odd_body"
  "odd_even_pe ''even'' = Some even_body"
  by (auto simp: odd_even_pe_def)  

  
lemma "SPECS odd_even_pe {
    (\<lambda>_. True, ''odd'', \<lambda>g\<^sub>0 g. g ''R'' \<noteq> 0 \<longleftrightarrow> odd (g\<^sub>0 ''par1'')),
    (\<lambda>_. True, ''even'', \<lambda>g\<^sub>0 g. g ''R'' \<noteq> 0 \<longleftrightarrow> even (g\<^sub>0 ''par1''))
  }"
  apply (rule specI[where R="measure (\<lambda>(g,_). nat (abs (g ''par1'')))"])
  apply simp
  apply (auto simp add: odd_even_pe_def) []
  apply (clarsimp; elim disjE; clarsimp)
  subgoal for g\<^sub>0
    unfolding odd_body_def
    apply (intro wpI_rules)
    apply simp
    apply simp
    apply (rule use_RECSPECSI, assumption)
    apply simp
    apply simp
    apply simp
    apply simp
    apply (rule use_RECSPECSI, assumption)
    apply simp
    apply simp
    apply simp
    apply simp
    done
  subgoal for g\<^sub>0
    unfolding even_body_def
    apply (intro wpI_rules)
    apply simp
    apply (rule use_RECSPECSI, assumption, (simp;fail))
    apply simp
    apply simp
    apply simp
    apply (rule use_RECSPECSI, assumption, (simp;fail))
    apply simp
    apply simp
    apply simp
    done
  done
    
      
subsubsection \<open>Homeier's Cycling Termination\<close>  

text \<open>A contrived example from Homeier's thesis. 
  Only the termination proof is interesting, no useful results are computed.\<close>

definition "pedal_body \<equiv> proc2 ''n'' ''m'' (
  IF Less (N 0) (VL ''n'') THEN (
    IF Less (N 0) (VL ''m'') THEN 
      vcall2 ''coast'' (Minus (VL ''n'') (N 1)) (Minus (VL ''m'') (N 1))
    ELSE SKIP;;
    vcall2 ''pedal'' (Minus (VL ''n'') (N 1)) (VL ''m'')
  ) ELSE SKIP)"
  
definition "coast_body \<equiv> proc2 ''n'' ''m'' (
  vcall2 ''pedal'' (VL ''n'') (VL ''m'');;
  IF (Less (N 0) (VL ''m'')) THEN
    vcall2 ''coast'' (VL ''n'') (Minus (VL ''m'') (N 1))
  ELSE SKIP
)"  

definition "pedal_coast_pe \<equiv> [''pedal'' \<mapsto> pedal_body, ''coast'' \<mapsto> coast_body]"

lemma [simp]:
  "pedal_coast_pe ''pedal'' = Some pedal_body"
  "pedal_coast_pe ''coast'' = Some coast_body"
  by (auto simp: pedal_coast_pe_def)
  
lemma "SPECS pedal_coast_pe {
  (\<lambda>g. g ''par1''\<ge>0 \<and> g ''par2''\<ge>0, ''pedal'',\<lambda>_ _. True),
  (\<lambda>g. g ''par1''\<ge>0 \<and> g ''par2''\<ge>0, ''coast'',\<lambda>_ _. True)
  }"
  (* The termination measure is straightforward, as proposed by Homeier: 
    n+m+p, where p is 1 if the call goes to coast, and 0 if the call goes to pedal *)
  apply (rule specI[where R="measure (\<lambda>(g,p). nat (
    g ''par1'' + g ''par2'' +
    (if p=''pedal'' then 0 else 1)
  ))"])
  apply auto []
  apply auto []
  apply (clarsimp; elim disjE; clarsimp)
  
  subgoal for g\<^sub>0
    unfolding pedal_body_def
    apply (intro wpI_rules; simp?)
    apply (rule use_RECSPECSI, assumption, (simp;fail))
    apply auto []
    apply auto []
    apply (intro wpI_rules; simp?)
    apply (rule use_RECSPECSI, assumption, (simp;fail))
    apply auto []
    apply auto []
    apply auto []
    apply (rule use_RECSPECSI, assumption, (simp;fail))
    apply auto []
    apply auto []
    apply auto []
    done
    
  subgoal for g\<^sub>0  
    unfolding coast_body_def
    by (
      intro wpI_rules; simp?
    | (rule use_RECSPECSI, assumption, (simp;fail))
    | ((auto) [])
    )+
  done  
  

subsection \<open>VCG based Proofs\<close>    
        
subsubsection \<open>Fibonacci\<close>
lemma fib_spec: "spec fib_pe (\<lambda>_. True, ''fib'', \<lambda>g\<^sub>0 g. g ''R'' = ifib (g\<^sub>0 ''par1''))"
  apply (rule spec1I[where R="measure (\<lambda>g. nat (g ''par1''))"])
  apply vcg unfolding fib_def by vcg
    
subsubsection \<open>Odd/Even\<close>
lemma odd_even_specs: "SPECS odd_even_pe {
    (\<lambda>_. True, ''odd'', \<lambda>g\<^sub>0 g. (g ''R'' = 1 \<longleftrightarrow> odd (g\<^sub>0 ''par1'')) \<and> g\<^sub>0 = g on -{''R'',''par1''} ),
    (\<lambda>_. True, ''even'', \<lambda>g\<^sub>0 g. (g ''R'' = 1 \<longleftrightarrow> even (g\<^sub>0 ''par1'')) \<and> g\<^sub>0 = g on -{''R'',''par1''} )
  }"
  apply (rule specI[where R="measure (\<lambda>(g,_). nat (abs (g ''par1'')))"])
  apply vcg unfolding odd_body_def even_body_def
  by vcg

subsubsection \<open>Cycling Termination\<close>
lemma "SPECS pedal_coast_pe {
  (\<lambda>g. g ''par1''\<ge>0 \<and> g ''par2''\<ge>0, ''pedal'',\<lambda>_ _. True),
  (\<lambda>g. g ''par1''\<ge>0 \<and> g ''par2''\<ge>0, ''coast'',\<lambda>_ _. True)
  }"
  (* The termination measure is straightforward, as proposed by Homeier: 
    n+m+p, where p is 1 if the call goes to coast, and 0 if the call goes to pedal *)
  apply (rule specI[where R="measure (\<lambda>(g,p). nat (
    g ''par1'' + g ''par2'' +
    (if p=''pedal'' then 0 else 1)
  ))"])
  apply vcg unfolding pedal_body_def coast_body_def
  apply vcg
  done  

subsubsection \<open>Simple f,g example\<close>  
context begin
private function f :: "int \<Rightarrow> unit" and g :: "int \<Rightarrow> unit" where
  "f i = g (i+1)" | "g i = (if i<2 then () else f (i-2))"
  by pat_completeness auto
  
termination  
  apply (relation "measure (nat o (\<lambda>Inl i \<Rightarrow> if i<0 then 1 else 2*i+3 | Inr i \<Rightarrow> if i<0 then 0 else 2*i))")
  by auto
end

lemma "SPECS [
  ''f''\<mapsto>SCALL ''g'', 
  ''g''\<mapsto> proc1 ''n'' (
      IF (Less (VL ''n'') (N 1)) THEN SKIP 
      ELSE vcall1 ''f'' (Minus (VL ''n'') (N 1)))
  ]
  {(\<lambda>_. True,''f'',\<lambda>_ _. True), (\<lambda>_. True,''g'',\<lambda>_ _. True)}
  "
  apply (rule specI[where R="measure (\<lambda>(g,p). 2*nat (g ''par1'') + (if p=''f'' then 1 else 0))"])
  by vcg
  
lemma "SPECS [
  ''f''\<mapsto>proc1 ''n'' (vcall1 ''g'' (Plus (VL ''n'') (N 1))), 
  ''g''\<mapsto> proc1 ''n'' (
      IF (Less (VL ''n'') (N 2)) THEN SKIP 
      ELSE vcall1 ''f'' (Minus (VL ''n'') (N 2)))
  ]
  {(\<lambda>_. True,''f'',\<lambda>_ _. True), (\<lambda>_. True,''g'',\<lambda>_ _. True)}
  "
  apply (rule specI[where R="measure (\<lambda>(g,p). nat (let n = g ''par1'' in
    if n<0 then 
      if p=''f'' then 1 else 0
    else
      2*n + (if p=''f'' then 3 else 0)
    )
    
    
    )"])
    supply Let_def[simp]  
    by vcg
  
      
subsubsection \<open>Ackermann function\<close>
  
fun ack :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "ack 0 n = n+1"
| "ack m 0 = ack (m-1) 1"
| "ack m n = ack (m-1) (ack m (n-1))"
  
lemma ack_simp: "ack m n 
  = (if m=0 then n+1 else if n=0 then ack (m-1) 1 else ack (m-1) (ack m (n-1)))"
  apply (cases "(m,n)" rule: ack.cases)
  by (auto)

value \<open>ack 3 2\<close>

definition "ack_body \<equiv> proc2 ''m'' ''n'' (
  IF Equal (VL ''m'') (N 0) THEN ''R'' G::= Plus (VL ''n'') (N 1)
  ELSE IF (Equal (VL ''n'') (N 0)) THEN vcall2 ''ack'' (Minus (VL ''m'') (N 1)) (N 1)
  ELSE (
    call2 ''r'' ''ack'' (VL ''m'') (Minus (VL ''n'') (N 1));;
    vcall2 ''ack'' (Minus (VL ''m'') (N 1)) (VL ''r'')
  )
)"

definition "ack_pe \<equiv> [''ack'' \<mapsto> ack_body]"
lemma [simp]: "ack_pe ''ack'' = Some ack_body" by (auto simp: ack_pe_def)

lemma [simp]: "\<lbrakk> 0 \<le> m; m \<noteq> 0 \<rbrakk> \<Longrightarrow> ack (nat m) 0 = ack (nat (m - 1)) (Suc 0)"
  by (cases "nat m") (auto simp: nat_diff_distrib') 

lemma [simp]: "\<lbrakk>0\<le>m; 0\<le>n; m\<noteq>0; n\<noteq>0\<rbrakk> 
  \<Longrightarrow> ack (nat m) (nat n) = ack (nat (m-1)) (ack (nat m) (nat (n-1)))"  
  apply (cases "(nat m, nat n)" rule: ack.cases)
  apply (auto simp: nat_diff_distrib')
  done


lemma "spec ack_pe 
  (\<lambda>g. g ''par1'' \<ge> 0 \<and> g ''par2'' \<ge> 0, 
     ''ack'', 
   \<lambda>g\<^sub>0 g. g ''R'' = int (ack (nat (g\<^sub>0 ''par1'')) (nat (g\<^sub>0 ''par2''))))"
  apply (rule spec1I[where 
    R="(\<lambda>g. nat (g ''par1'')) <*mlex*> (\<lambda>g. nat (g ''par2'')) <*mlex*> {}"])
  supply mlex_prod_def[simp]  
  apply vcg
  unfolding ack_body_def
  by vcg
  
  
subsubsection \<open>McCarthy's 91 Function\<close>
text \<open>A standard benchmark for verification of recursive functions.
  We use Homeier's version with a global variable.\<close>

definition "p91_body \<equiv> proc1 ''y'' (
  IF (Less (N 100) (VL ''y'')) THEN ''x'' G::= Minus (VL ''y'') (N 10)
  ELSE (
    vcall1 ''p91'' (Plus (VL ''y'') (N 11));;
    vcall1 ''p91'' (VG ''x'')
  )
)"

definition "p91_pe \<equiv> [''p91'' \<mapsto> p91_body]"
lemma [simp]: "p91_pe ''p91'' = Some p91_body" by (auto simp: p91_pe_def)

lemma "spec p91_pe (\<lambda>g. True, ''p91'', \<lambda>g\<^sub>0 g. if 100 < g\<^sub>0 ''par1'' then g ''x'' = g\<^sub>0 ''par1'' - 10 else g ''x'' = 91)"
  apply (rule spec1I[where R="measure (\<lambda>g. nat (101 - g ''par1''))"])
  apply (vcg)
  unfolding p91_body_def
  supply if_splits[split]
  apply vcg
  done
  
  
          
subsection \<open>Modularity Example\<close>

subsubsection \<open>Counting odd Fibonacci Numbers\<close>
text \<open>Re-using already proved modules in new program.
  The example is completely ad-hoc, chosen to combine two different modules.
\<close>
  
definition "oddfibnum_body \<equiv> proc1 ''n'' (
  ''i'' ::= N 0;;
  ''c'' ::= N 0;;
  
  WHILE (Less (VL ''i'') (VL ''n'')) DO (
    call1 ''f'' ''fib'' (VL ''i'');;
    call1 ''is_odd'' ''odd'' (VL ''f'');;
    IF Equal (VL ''is_odd'') (N 1) THEN
      ''c'' ::= Plus (VL ''c'') (N 1)
    ELSE SKIP;;
    ''i'' ::= Plus (VL ''i'') (N 1)
  );;
  ''R'' G::= VL ''c''
)"  
  
text \<open>
  This is roughly: \<open>
    module oddfibnum_pe
      import <fib> 
      import <odd_even>
      declare oddfibnum ();
    \<close>
\<close>
definition "oddfibnum_pe \<equiv> fib_pe ++ odd_even_pe ++ [''oddfibnum'' \<mapsto> oddfibnum_body]"

lemma [simp]: "oddfibnum_pe ''oddfibnum'' = Some oddfibnum_body"
  by (auto simp: oddfibnum_pe_def)
  
  
text \<open>We show that our module does not introduce duplicate names\<close>  
lemma oddfibnum_pe_compat:
  "fib_pe \<subseteq>\<^sub>m oddfibnum_pe"  
  "odd_even_pe \<subseteq>\<^sub>m oddfibnum_pe"  
  unfolding fib_pe_def odd_even_pe_def oddfibnum_pe_def
  by auto
  
text \<open>And import the specifications of the original module\<close>  
lemmas oddfibnum_lifted_specs = 
  lift_spec[OF oddfibnum_pe_compat(1) fib_spec]  
  lift_SPECS[OF oddfibnum_pe_compat(2) odd_even_specs]

text \<open>
  Note that all the above is canonical boilerplate code, which could be handled automatically!
  
  We could even easily go for fancier things like 
    \<open>import qualified mname\<close>
  which would prefix all the function names imported from module mname by \<open>mname.\<close>.
  
  Hiding certain functions as module-private cannot be directly done, but we can rename 
  a private function \<open>fun\<close> in module \<open>mod\<close> to something like \<open>mod.private.fun\<close>.
  
  Exercise: Provide a function \<open>rename:: (string \<Rightarrow> string) \<Rightarrow> penv \<Rightarrow> penv\<close> that renames
    all procedures in a procedure environment (module), and show that specifications are preserved.
    
    \<open>spec pe (P,p,Q) \<Longrightarrow> spec (rename \<rho> pe) (P,\<rho> p,Q)\<close>
    
    What conditions must the renaming satisfy?
  
\<close>
  
  
    
definition "oddfibs n \<equiv> int (length (filter odd (map (ifib o int) [0..<nat n])))"
lemma [simp]: 
  "oddfibs 0 = 0"
  "0\<le>i \<Longrightarrow> odd (ifib i) \<Longrightarrow> oddfibs (i+1) = oddfibs i + 1"
  "0\<le>i \<Longrightarrow> even (ifib i) \<Longrightarrow> oddfibs (i+1) = oddfibs i"
  by (auto simp: oddfibs_def nat_add_distrib)

lemma "spec oddfibnum_pe (\<lambda>g. g ''par1'' \<ge> 0,''oddfibnum'',\<lambda>g\<^sub>0 g. g ''R'' = oddfibs (g\<^sub>0 ''par1''))"
  apply (rule specI_nonrec)
  apply simp
  unfolding oddfibnum_body_def
  apply vcg
  apply (rule wpI_while[where 
    R="measure (\<lambda>(l,g). nat (l ''n'' - l ''i''))" and
    I="\<lambda>(l\<^sub>0,g\<^sub>0) (l,g). 0\<le>l ''i'' \<and> l ''i'' \<le> l ''n'' \<and> l\<^sub>0 ''n'' = l ''n'' \<and> l ''c'' = oddfibs (l ''i'')"
    ])
  apply vcg  
  using oddfibnum_lifted_specs 
  apply -
  apply vcg
  done
  
  
  
subsubsection \<open>Pandya and Joseph's Product Producers\<close>
text \<open>Again, taking the version from Homeier's thesis, but with a 
  modification to also terminate for negative \<open>y\<close>.
  
  This is a very contrived example, and illustrates some points that
  automation and tooling could be improved on.
\<close>

definition "product_body \<equiv> 
  call1 ''e'' ''even'' (VG ''y'');;
  IF Equal (VL ''e'') (N 1) THEN SCALL ''evenproduct'' 
                            ELSE SCALL ''oddproduct''
"

definition "oddproduct_body \<equiv> 
  IF (Less (VG ''y'') (N 0)) THEN (
    ''y'' G::= Plus (VG ''y'') (N 1);;
    ''z'' G::= Minus (VG ''z'') (VG ''x'')
  ) ELSE (
    ''y'' G::= Minus (VG ''y'') (N 1);;
    ''z'' G::= Plus (VG ''z'') (VG ''x'')
  )  
  ;;
  SCALL ''evenproduct''
"

definition "evenproduct_body \<equiv> 
  IF Equal (VG ''y'') (N 0) THEN SKIP
  ELSE (
    ''x'' G::= Times (N 2) (VG ''x'');;
    ''y'' G::= Div (VG ''y'') (N 2);;
    SCALL ''product''
  )
"

definition "pjpp_pe \<equiv> odd_even_pe ++ [
  ''product'' \<mapsto> product_body,
  ''oddproduct'' \<mapsto> oddproduct_body,
  ''evenproduct'' \<mapsto> evenproduct_body
]"  

lemma [simp]:
  "pjpp_pe ''product'' = Some product_body"
  "pjpp_pe ''oddproduct'' = Some oddproduct_body"
  "pjpp_pe ''evenproduct'' = Some evenproduct_body"
  unfolding pjpp_pe_def by auto

lemma pjpp_pe_compat:
  "odd_even_pe \<subseteq>\<^sub>m pjpp_pe"  
  unfolding odd_even_pe_def pjpp_pe_def
  by auto
  
text \<open>And import the specifications of the original module\<close>  
lemmas pjpp_lifted_specs = 
  lift_SPECS[OF pjpp_pe_compat odd_even_specs]
  
(** We don't have automatic call-graph reasoning, so we have to prove things like this by hand ... for now*)
lemma [simp]: "{(''oddproduct'', ''product'')}\<^sup>* = insert (''oddproduct'', ''product'') Id"  
  apply auto
  apply (meson converse_rtranclE prod.inject singletonD)
  by (metis Pair_inject rtrancl.cases singletonD)
  
lemma [simp]: "odd (y::int) \<Longrightarrow> \<not>y<0 \<longleftrightarrow> 0<y" by presburger 
lemma [simp]: "even (y::int) \<Longrightarrow> y \<noteq> 0 \<Longrightarrow> \<bar>y div 2\<bar> < \<bar>y\<bar>" by presburger  
  
lemma "SPECS pjpp_pe {
  (\<lambda>g. True, ''product'', \<lambda>g\<^sub>0 g. g ''z'' = g\<^sub>0 ''z'' + g\<^sub>0 ''x'' * g\<^sub>0 ''y''),
  (\<lambda>g. odd (g ''y''), ''oddproduct'', \<lambda>g\<^sub>0 g. g ''z'' = g\<^sub>0 ''z'' + g\<^sub>0 ''x'' * g\<^sub>0 ''y''),
  (\<lambda>g. even (g ''y''), ''evenproduct'', \<lambda>g\<^sub>0 g. g ''z'' = g\<^sub>0 ''z'' + g\<^sub>0 ''x'' * g\<^sub>0 ''y'')
}"
  apply (rule specI[where R="(\<lambda>(g,_). nat (abs (g ''y''))) 
    <*mlex*> inv_image ({''evenproduct'',''oddproduct''}\<times>{''product''}) snd"])
  supply mlex_prod_def[simp] eq_on_def[simp] algebra_simps[simp]
  apply vcg
  using pjpp_lifted_specs apply -
  unfolding product_body_def oddproduct_body_def evenproduct_body_def 
  by vcg
      

end
