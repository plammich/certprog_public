(* Author: Peter Lammich. Derived from theory of Tobias Nipkow. *)

subsection \<open>Soundness and Completeness for Partial Correctness\<close>

theory Hoare_Sound_Complete
imports Big_Step Hoare
begin


subsubsection \<open>Deeply Embedded Hoare Calculus\<close>
inductive
  hoare :: "assn \<Rightarrow> com \<Rightarrow> assn \<Rightarrow> bool" ("\<turnstile> ({(1_)}/ (_)/ {(1_)})" 50)
where
Skip: "\<turnstile> {P} SKIP {P}"  |

Assign:  "\<turnstile> {\<lambda>s. P(s(x:=aval a s))} x::=a {P}"  |

Seq: "\<lbrakk> \<turnstile> {P} c\<^sub>1 {Q};  \<turnstile> {Q} c\<^sub>2 {R} \<rbrakk>
      \<Longrightarrow> \<turnstile> {P} c\<^sub>1;;c\<^sub>2 {R}"  |

If: "\<lbrakk> \<turnstile> {\<lambda>s. P s \<and> bval b s} c\<^sub>1 {Q};  \<turnstile> {\<lambda>s. P s \<and> \<not> bval b s} c\<^sub>2 {Q} \<rbrakk>
     \<Longrightarrow> \<turnstile> {P} IF b THEN c\<^sub>1 ELSE c\<^sub>2 {Q}"  |

While: "\<turnstile> {\<lambda>s. P s \<and> bval b s} c {P} \<Longrightarrow>
        \<turnstile> {P} WHILE b DO c {\<lambda>s. P s \<and> \<not> bval b s}"  |

conseq: "\<lbrakk> \<forall>s. P' s \<longrightarrow> P s;  \<turnstile> {P} c {Q};  \<forall>s. Q s \<longrightarrow> Q' s \<rbrakk>
        \<Longrightarrow> \<turnstile> {P'} c {Q'}"

lemmas [simp] = hoare.Skip hoare.Assign hoare.Seq If

lemmas [intro!] = hoare.Skip hoare.Assign hoare.Seq hoare.If

lemma strengthen_pre:
  "\<lbrakk> \<forall>s. P' s \<longrightarrow> P s;  \<turnstile> {P} c {Q} \<rbrakk> \<Longrightarrow> \<turnstile> {P'} c {Q}"
by (blast intro: conseq)

lemma weaken_post:
  "\<lbrakk> \<turnstile> {P} c {Q};  \<forall>s. Q s \<longrightarrow> Q' s \<rbrakk> \<Longrightarrow>  \<turnstile> {P} c {Q'}"
by (blast intro: conseq)



subsubsection "Soundness"

text \<open>Soundness is straightforward, essentially we have already proved 
  the rules as lemmas.\<close>
lemma hoare_sound: "\<turnstile> {P}c{Q}  \<Longrightarrow>  \<Turnstile> {P}c{Q}"
  apply (induction rule: hoare.induct)
  apply (rule skip_rule assign_rule seq_rule if_rule basic_while_rule conseq_rule; assumption)+
  by (blast intro: conseq_rule)


subsubsection "Weakest Liberal Precondition"

definition wlp :: "com \<Rightarrow> assn \<Rightarrow> assn" where
"wlp c Q = (\<lambda>s. \<forall>t. (c,s) \<Rightarrow> t  \<longrightarrow>  Q t)"

text \<open>Wlp is a precondition\<close>
lemma wlp_is_pre: "\<Turnstile> {wlp c Q} c {Q}" by (auto simp: wlp_def hoare_valid_def)

text \<open>Wlp is the weakest one, i.e., implied by all other preconditions\<close>
lemma wlp_weakest: "\<Turnstile> {P} c {Q} \<Longrightarrow> P s \<Longrightarrow> wlp c Q s" 
  by (auto simp: wlp_def hoare_valid_def dest: big_step_determ)

text \<open>Phrased as an equivalence: A Hoare-triple is valid, 
  iff its precondition implies the weakest precondition of 
  command an postcondition.\<close>
lemma wlp_hoare_iff: "\<Turnstile> {P} c {Q} \<longleftrightarrow> (\<forall>s. P s \<longrightarrow> wlp c Q s)"  
  using wlp_is_pre[of c Q] wlp_weakest[of P c Q] 
  by (auto intro: conseq_pre_rule)
  
lemma wlp_mono: "(\<forall>t. P t \<longrightarrow> Q t) \<Longrightarrow> wlp c P s \<Longrightarrow> wlp c Q s" 
  by (auto simp: wlp_def)
  

lemma wlp_SKIP[simp]: "wlp SKIP Q = Q"
by (rule ext) (auto simp: wlp_def)

lemma wlp_Ass[simp]: "wlp (x::=a) Q = (\<lambda>s. Q(s(x:=aval a s)))"
by (rule ext) (auto simp: wlp_def)

lemma wlp_Seq[simp]: "wlp (c\<^sub>1;;c\<^sub>2) Q = wlp c\<^sub>1 (wlp c\<^sub>2 Q)"
by (rule ext) (auto simp: wlp_def)

lemma wlp_If[simp]:
 "wlp (IF b THEN c\<^sub>1 ELSE c\<^sub>2) Q =
 (\<lambda>s. if bval b s then wlp c\<^sub>1 Q s else wlp c\<^sub>2 Q s)"
by (rule ext) (auto simp: wlp_def)

lemma wlp_While_If:
 "wlp (WHILE b DO c) Q = 
  wlp (IF b THEN c;;WHILE b DO c ELSE SKIP) Q"
unfolding wlp_def by (metis unfold_while)

subsubsection "Completeness"

text \<open>If we can derive \<open>wlp\<close>, we can derive any other valid property \<close>
lemma derive_wlp_complete:
  assumes 1: "\<turnstile> {wlp c Q} c {Q}"
  assumes 2: "\<Turnstile> {P} c {Q}"
  shows "\<turnstile> {P} c {Q}"
proof -
  from 2 have "\<forall>s. P s \<longrightarrow> wlp c Q s" using wlp_weakest by blast
  from strengthen_pre[OF this 1] show ?thesis .   
qed    
(*  by (metis assms strengthen_pre wlp_weakest) *)

  
text \<open>Idea: @{term "wlp (WHILE b DO c) Q"} is invariant\<close>
lemma wlp_is_invar:
  fixes b c
  defines "w \<equiv> WHILE b DO c"
  shows "\<Turnstile> {\<lambda>s. wlp w Q s \<and> bval b s} c {wlp w Q}"
proof (clarsimp simp: wlp_hoare_iff )
  fix s
  assume "bval b s"
  assume "wlp w Q s" 
  hence "wlp (IF b THEN c;;w ELSE SKIP) Q s"
    unfolding w_def by (simp add: wlp_While_If)
  then show "wlp c (wlp w Q) s" using \<open>bval b s\<close> by simp
qed    
  
lemma "\<not>bval b s \<Longrightarrow> wlp (WHILE b DO c) Q s \<longleftrightarrow> Q s"
  apply (subst wlp_While_If)
  by simp  
  
  

lemma derive_wlp: "\<turnstile> {wlp c Q} c {Q}"
proof(induction c arbitrary: Q)
  case If thus ?case by(auto intro: conseq)
next
  case (While b c)
  let ?w = "WHILE b DO c"
  
  from derive_wlp_complete[OF While.IH wlp_is_invar]
  have wlp_invar: "\<turnstile> {\<lambda>s. wlp ?w Q s \<and> bval b s} c {wlp ?w Q}" .

  from hoare.While[OF wlp_invar] have "\<turnstile> {wlp ?w Q} ?w {\<lambda>s. wlp ?w Q s \<and> \<not> bval b s}" .
  moreover have "\<forall>s. wlp ?w Q s \<and> \<not> bval b s \<longrightarrow> Q s"
    by (auto simp: wlp_While_If)
  ultimately show ?case using conseq by blast
qed auto

lemma hoare_complete: "\<Turnstile> {P}c{Q} \<Longrightarrow> \<turnstile> {P}c{Q}"
  using derive_wlp_complete[OF derive_wlp] .

corollary hoare_sound_complete: "\<turnstile> {P}c{Q} \<longleftrightarrow> \<Turnstile> {P}c{Q}"
by (metis hoare_complete hoare_sound)




subsection \<open>Soundness and Completeness for Total Correctness\<close>

subsubsection \<open>Deeply Embedded Hoare Calculus\<close>
inductive
  hoaret :: "assn \<Rightarrow> com \<Rightarrow> assn \<Rightarrow> bool" ("\<turnstile>\<^sub>t ({(1_)}/ (_)/ {(1_)})" 50)
where
Skip: "\<turnstile>\<^sub>t {P} SKIP {P}"  |

Assign:  "\<turnstile>\<^sub>t {\<lambda>s. P(s(x:=aval a s))} x::=a {P}"  |

Seq: "\<lbrakk> \<turnstile>\<^sub>t {P} c\<^sub>1 {Q};  \<turnstile>\<^sub>t {Q} c\<^sub>2 {R} \<rbrakk>
      \<Longrightarrow> \<turnstile>\<^sub>t {P} c\<^sub>1;;c\<^sub>2 {R}"  |

If: "\<lbrakk> \<turnstile>\<^sub>t {\<lambda>s. P s \<and> bval b s} c\<^sub>1 {Q};  \<turnstile>\<^sub>t {\<lambda>s. P s \<and> \<not> bval b s} c\<^sub>2 {Q} \<rbrakk>
     \<Longrightarrow> \<turnstile>\<^sub>t {P} IF b THEN c\<^sub>1 ELSE c\<^sub>2 {Q}"  |

While: "wf R \<Longrightarrow> (\<And>s\<^sub>0. \<turnstile>\<^sub>t {\<lambda>s. P s \<and> bval b s \<and> s=s\<^sub>0} c {\<lambda>s. P s \<and> (s,s\<^sub>0)\<in>R}) \<Longrightarrow>
        \<turnstile>\<^sub>t {P} WHILE b DO c {\<lambda>s. P s \<and> \<not> bval b s}"  |

conseq: "\<lbrakk> \<forall>s. P' s \<longrightarrow> P s;  \<turnstile>\<^sub>t {P} c {Q};  \<forall>s. Q s \<longrightarrow> Q' s \<rbrakk>
        \<Longrightarrow> \<turnstile>\<^sub>t {P'} c {Q'}"
        
lemmas [simp] = hoaret.Skip hoaret.Assign hoaret.Seq If

lemmas [intro!] = hoaret.Skip hoaret.Assign hoaret.Seq hoaret.If

lemma strengthen_pre_t:
  "\<lbrakk> \<forall>s. P' s \<longrightarrow> P s;  \<turnstile>\<^sub>t {P} c {Q} \<rbrakk> \<Longrightarrow> \<turnstile>\<^sub>t {P'} c {Q}"
by (blast intro: conseq)


lemma weaken_post_t:
  "\<lbrakk> \<turnstile>\<^sub>t {P} c {Q};  \<forall>s. Q s \<longrightarrow> Q' s \<rbrakk> \<Longrightarrow>  \<turnstile>\<^sub>t {P} c {Q'}"
by (blast intro: conseq)



subsubsection "Soundness"

text \<open>Soundness is straightforward, essentially we have already proved 
  the rules as lemmas.\<close>
lemma hoaret_sound: "\<turnstile>\<^sub>t {P}c{Q}  \<Longrightarrow>  \<Turnstile>\<^sub>t {P}c{Q}"
  apply (induction rule: hoaret.induct)
  apply (rule skip_trule assign_trule seq_trule if_trule basic_while_trule conseq_trule; assumption)+
  by (blast intro: conseq_trule)


subsubsection "Weakest Precondition"

definition wp :: "com \<Rightarrow> assn \<Rightarrow> assn" where
"wp c Q = (\<lambda>s. \<exists>t. (c,s) \<Rightarrow> t \<and> Q t)"

text \<open>Wp is a precondition\<close>
lemma wp_is_pre: "\<Turnstile>\<^sub>t {wp c Q} c {Q}" by (auto simp: wp_def hoaret_valid_def)

text \<open>Wp is the weakest one, i.e., implied by all other preconditions\<close>
lemma wp_weakest: "\<Turnstile>\<^sub>t {P} c {Q} \<Longrightarrow> P s \<Longrightarrow> wp c Q s" 
  by (auto simp: wp_def hoaret_valid_def dest: big_step_determ)

text \<open>Phrased as an equivalence: A Hoare-triple is valid, 
  iff its precondition implies the weakest precondition of 
  command and postcondition.\<close>
lemma wp_hoare_iff: "\<Turnstile>\<^sub>t {P} c {Q} \<longleftrightarrow> (\<forall>s. P s \<longrightarrow> wp c Q s)"  
  using wp_is_pre[of c Q] wp_weakest[of P c Q] 
  by (auto intro: conseq_pre_trule)
  
lemma wp_mono: "(\<forall>t. P t \<longrightarrow> Q t) \<Longrightarrow> wp c P s \<Longrightarrow> wp c Q s" 
  by (auto simp: wp_def)
  

lemma wp_SKIP[simp]: "wp SKIP Q = Q"
by (rule ext) (auto simp: wp_def)

lemma wp_Ass[simp]: "wp (x::=a) Q = (\<lambda>s. Q(s(x:=aval a s)))"
by (rule ext) (auto simp: wp_def)

lemma wp_Seq[simp]: "wp (c\<^sub>1;;c\<^sub>2) Q = wp c\<^sub>1 (wp c\<^sub>2 Q)"
by (rule ext) (auto simp: wp_def)

lemma wp_If[simp]:
 "wp (IF b THEN c\<^sub>1 ELSE c\<^sub>2) Q =
 (\<lambda>s. if bval b s then wp c\<^sub>1 Q s else wp c\<^sub>2 Q s)"
by (rule ext) (auto simp: wp_def)

lemma wp_While_If:
 "wp (WHILE b DO c) Q = 
  wp (IF b THEN c;;WHILE b DO c ELSE SKIP) Q"
unfolding wp_def by (metis unfold_while)

subsubsection "Completeness"

text \<open>If we can derive \<open>wp\<close>, we can derive any other valid property \<close>
lemma derive_wp_complete:
  assumes 1: "\<turnstile>\<^sub>t {wp c Q} c {Q}"
  assumes 2: "\<Turnstile>\<^sub>t {P} c {Q}"
  shows "\<turnstile>\<^sub>t {P} c {Q}"
proof -
  from 2 have "\<forall>s. P s \<longrightarrow> wp c Q s" using wp_weakest by blast
  from strengthen_pre_t[OF this 1] show ?thesis .   
qed    

  
text \<open>Idea: @{term "wp (WHILE b DO c) Q"} is invariant\<close>
lemma wp_is_invar:
  fixes b c
  defines "w \<equiv> WHILE b DO c"
  shows "\<Turnstile>\<^sub>t {\<lambda>s. wp w Q s \<and> bval b s} c {wp w Q}"
proof (clarsimp simp: wp_hoare_iff )
  fix s
  assume "bval b s"
  assume "wp w Q s" 
  hence "wp (IF b THEN c;;w ELSE SKIP) Q s"
    unfolding w_def by (simp add: wp_While_If)
  then show "wp c (wp w Q) s" using \<open>bval b s\<close> by simp
qed    
  
lemma wp_term_imp_Q: "\<not>bval b s \<Longrightarrow> wp (WHILE b DO c) Q s \<longleftrightarrow> Q s"
  apply (subst wp_While_If)
  by simp  
  
text \<open>Idea: 
  As wf-relation, use all possible state transitions of loop body, 
  for states that satisfy the invariant (\<open>wp w Q\<close>) and the loop condition.
\<close>  
  
definition termrel :: "bexp \<Rightarrow> com \<Rightarrow> assn \<Rightarrow> (state\<times>state) set" where
  "termrel b c Q \<equiv> {(t,s). wp (WHILE b DO c) Q s \<and> bval b s \<and> (c,s) \<Rightarrow> t }"

text \<open>This obviously satisfies the variant condition from our Hoare rule\<close>
lemma termrel_is_variant: 
  "\<Turnstile> {\<lambda>s. wp (WHILE b DO c) Q s \<and> bval b s \<and> s=s\<^sub>0} 
        c 
      {\<lambda>s. (s,s\<^sub>0)\<in>termrel b c Q}"
  by (auto simp: termrel_def hoare_valid_def)
  
text \<open>Note that we did not bother to show termination (again), 
  as we already know that c will terminate when \<open>wp w Q\<close> holds.
  
  In general, when combining multiple Hoare-triples for the same program, 
  only one needs to prove termination:
  \<close>
  
(* Make this a Quiz during lecture. 
  Why do we need big_step_determ for last proof? 
*)  
  
lemma combine_partial_total:  
  assumes "\<Turnstile>\<^sub>t {P} c {Q}"
  assumes "\<Turnstile> {P'} c {Q'}"
  shows "\<Turnstile>\<^sub>t {\<lambda>s. P s \<and> P' s} c {\<lambda>s. Q s \<and> Q' s}"
  using assms unfolding hoare_valid_def hoaret_valid_def by blast

lemma combine_partial_partial:  
  assumes "\<Turnstile> {P} c {Q}"
  assumes "\<Turnstile> {P'} c {Q'}"
  shows "\<Turnstile> {\<lambda>s. P s \<and> P' s} c {\<lambda>s. Q s \<and> Q' s}"
  using assms unfolding hoare_valid_def hoaret_valid_def by blast
  
lemma combine_total_total:  
  assumes "\<Turnstile>\<^sub>t {P} c {Q}"
  assumes "\<Turnstile>\<^sub>t {P'} c {Q'}"
  shows "\<Turnstile>\<^sub>t {\<lambda>s. P s \<and> P' s} c {\<lambda>s. Q s \<and> Q' s}"
  using assms unfolding hoare_valid_def hoaret_valid_def 
  using big_step_determ by blast
  
  
text \<open>So we can combine invariant and variant property,
  as required by our Hoare rule.\<close>  
  
lemma wp_combined_invar: 
  fixes b c
  defines "w \<equiv> WHILE b DO c"
  shows "\<Turnstile>\<^sub>t {\<lambda>s. wp w Q s \<and> bval b s \<and> s=s\<^sub>0} c {\<lambda>s. wp w Q s \<and> (s,s\<^sub>0)\<in>termrel b c Q}"
proof -
  note 1 = combine_partial_total[OF 
    wp_is_invar[of b c Q]
    termrel_is_variant[of b c Q]
  ]
  note 2 = conseq_trule[OF _ 1]
  show ?thesis
    apply (rule 2)
    unfolding w_def
    by blast+
qed  

text \<open>
  Finally, we need to show that our relation is well-founded.
  
  Recall, that well-foundedness means that there are no infinite chains.
\<close>
thm wf_iff_no_infinite_down_chain

text \<open>
  Intuitively, we must have no sequence of states
  \<open>s\<^sub>0 s\<^sub>1 s\<^sub>2 \<dots>\<close> such that \<open>(s\<^sub>i\<^sub>+\<^sub>1,s\<^sub>i) \<in> R\<close>

  We represent an infinite sequence of states by a function 
  \<open>s::nat \<Rightarrow> state\<close>, and by contrapositive, we get the following lemma
  to prove well-foundedness:
\<close>

lemma wf_no_inf_downchainI:
  assumes "\<And>(s::nat\<Rightarrow>'a). (\<forall>i. (s (Suc i), s i)\<in>R) \<Longrightarrow> False"
  shows "wf R"
  unfolding wf_iff_no_infinite_down_chain 
  using assms
  by blast

  
text \<open>We start with two auxiliary lemmas:
  First,
  if we have a sequence of states \<open>s\<^sub>0 s\<^sub>1 \<dots>\<close> such that
  \<open>(c,s\<^sub>i) \<Rightarrow> s\<^sub>i\<^sub>+\<^sub>1\<close>, and the execution of the while loop from \<open>s\<^sub>0\<close>
  terminates in some state, then this state must be among 
  the \<open>s\<^sub>i\<close>. This is because execution is deterministic.
  
  For induction to go through, we generalize this lemma over the index of 
  the start state of the execution:
\<close>  
  
lemma loop_result_in_chain:
  assumes c_chain: "\<forall>i. (c, s i) \<Rightarrow> s (Suc i)"
  assumes loop_terminates: "(WHILE b DO c,s i) \<Rightarrow> sf"
  shows "\<exists>j. sf = s j"
  using loop_terminates
  apply (induction "WHILE b DO c" "s i" sf arbitrary: i rule: big_step_induct)
  apply blast
  using c_chain big_step_determ by blast

text \<open>Second,
  if a loop terminates, the loop condition does not hold for the terminating state
\<close>
lemma loop_term_not_b:
  assumes "(WHILE b DO c,s) \<Rightarrow> t"
  shows "\<not>bval b t"
  using assms
  by (induction "WHILE b DO c" s t rule: big_step_induct) auto
  
  
lemma termrel_wf: "wf (termrel b c Q)"
proof (rule wf_no_inf_downchainI)  
  text \<open>We assume an infinite down-chain, and have to show a contradiction\<close>
  fix s 
  assume inf_downchain: "\<forall>i. (s (Suc i), s i) \<in> termrel b c Q"
  show False proof -
    
    text \<open>In particular, each \<open>s\<^sub>i\<^sub>+\<^sub>1\<close> satisfies \<open>b\<close>, 
      and is the result of executing \<open>c\<close> on \<open>s\<^sub>i\<close>\<close>
      
    from inf_downchain have   
      b_holds: "\<forall>i. bval b (s i)" and
      c_chain: "\<forall>i. (c, s i) \<Rightarrow> s (Suc i)"
      unfolding termrel_def by blast+
  
    let ?w = "WHILE b DO c"
    text \<open>Moreover, each \<open>s\<^sub>i\<close> satisfies \<open>wp ?w Q\<close>. 
      In particular, the while loop terminates for each \<open>s\<^sub>i\<close>.
      Let's pick \<open>s\<^sub>0\<close> as starting state and \<open>sf\<close> as terminating state
      \<close>
      
    from inf_downchain obtain sf where loop_terminates: "(?w,s 0) \<Rightarrow> sf"  
      unfolding termrel_def wp_def by blast
  
    text \<open>We have already shown that \<open>sf\<close> must be a state of the chain\<close>  
    then obtain i where [simp]: "sf = s i" 
      using loop_result_in_chain[OF c_chain] by blast
    
    text \<open>The loop condition does not hold for the terminating state\<close>
    from loop_terminates have "\<not>bval b (s i)" using loop_term_not_b by auto
    
    text \<open>But, we assumed the loop condition holds for any state in the chain\<close>
    with b_holds show False by blast
  qed
qed  


text \<open>The remaining proof is analogous to the partial-correctness version: \<close>

lemma derive_wp: "\<turnstile>\<^sub>t {wp c Q} c {Q}"
proof(induction c arbitrary: Q)
  case If thus ?case by(auto intro: conseq)
next
  case (While b c)
  let ?w = "WHILE b DO c"
  let ?R = "termrel b c Q"
  
  from derive_wp_complete[OF While.IH wp_combined_invar]
  have wp_invar: "\<turnstile>\<^sub>t {\<lambda>s. wp ?w Q s \<and> bval b s \<and> s=s\<^sub>0} c {\<lambda>s. wp ?w Q s \<and> (s,s\<^sub>0)\<in>?R}" for s\<^sub>0 .

  from hoaret.While[OF termrel_wf wp_invar]
  have "\<turnstile>\<^sub>t {wp (WHILE b DO c) Q} WHILE b DO c {\<lambda>s. wp (WHILE b DO c) Q s \<and> \<not> bval b s}" .
  moreover have "\<forall>s. wp ?w Q s \<and> \<not> bval b s \<longrightarrow> Q s"
    by (auto simp: wp_While_If)
  ultimately show ?case using conseq by blast
qed auto

lemma hoaret_complete: "\<Turnstile>\<^sub>t {P}c{Q} \<Longrightarrow> \<turnstile>\<^sub>t {P}c{Q}"
  using derive_wp_complete[OF derive_wp] .

corollary hoaret_sound_complete: "\<turnstile>\<^sub>t {P}c{Q} \<longleftrightarrow> \<Turnstile>\<^sub>t {P}c{Q}"
by (metis hoaret_complete hoaret_sound)



end
