theory Tutorial8
imports Main "../Project/IPP_Lib"
begin
  (* Simple stack machine *)
  datatype instr = 
      PUSH int (* Push constant on stack *)
    | ADD      (* Pop top two elements, then push their sum *) 

  (* Define a run-function, that executes a list of instructions
    on a given stack, and yields the resulting stack, or None on
    stack underflow.
  
    (medium..hard)
  *)
  fun run :: "instr list \<Rightarrow> int list \<Rightarrow> int list option" where
    "run [] s = Some s"
  | "run (PUSH x # is) s = run is (x#s)"
  | "run (ADD # is) (a#b#s) = run is ((a+b)#s)"  
  | "run _ _ = None"  
  
  (* Show: A run still works if we add something to the bottom of the stack 
    (medium)
  *)
  lemma "run l s = Some s' \<Longrightarrow> run l (s@t) = Some (s'@t)"
    by (induction l s rule: run.induct) auto  
  
  (* Define the sum of the pushed values in an instruction list 
    (medium)
  *)  
  fun sum_push :: "instr list \<Rightarrow> int" where 
    "sum_push [] = 0"
  | "sum_push (PUSH x # is) = x + sum_push is"  
  | "sum_push (ADD # is) = sum_push is"  
    
  (* Show that the sum of values on the new stack is the sum of 
    values on the old stack plus the sum of values pushed in the program 
    
    (medium)
    *)  
  lemma "run l s = Some s' \<Longrightarrow> sum_list s' = sum_list s + sum_push l"  
    by (induction l s rule: run.induct) auto  
  

  (*
    We define a simple assembly-like language.
    A program is a function from labels (nat) to commands.
    A command is either a jump, a state transition, or the command TERMINATE.
  *)  
    
  type_synonym state = "string \<Rightarrow> int"  
  datatype com = JMP nat | COMPUTE "state \<Rightarrow> state" | TERMINATE
  type_synonym program = "nat \<Rightarrow> com"

  (*
    This is a big-step semantics. After a COMPUTE-command, the next command is executed.
  *)
  inductive big_step :: "program \<Rightarrow> nat \<Rightarrow> state \<Rightarrow> state \<Rightarrow> bool" for prg where
    "prg pc = JMP pc' \<Longrightarrow> big_step prg pc' s s' \<Longrightarrow> big_step prg pc s s'"
  | "prg pc = COMPUTE f \<Longrightarrow> big_step prg (Suc pc) (f s) s' \<Longrightarrow> big_step prg pc s s'"
  | "prg pc = TERMINATE \<Longrightarrow> big_step prg pc s s"

  (* Show that the semantics is deterministic!  
    DO NOT USE SLEDGEHAMMER. (Your proof must not contain metis or smt!)
    Hint: Use an Isar-proof, as simp+auto will loop when applied naively!
    
    (hard)
  *)
  lemma "big_step prg pc s s' \<Longrightarrow> big_step prg pc s s'' \<Longrightarrow> s'=s''"
    apply (induction rule: big_step.induct)
    apply (auto elim: big_step.cases)
    done

  (*
    As we have no conditional jumps, termination of a program does not depend 
    on the states at all.
    
    Write an inductive predicate to characterize termination, and prove that 
    it is correct!

    (hard)
  *)
  inductive terminates :: "program \<Rightarrow> nat \<Rightarrow> bool" for prg where
    "prg pc = TERMINATE \<Longrightarrow> terminates prg pc"
  | "prg pc = COMPUTE f \<Longrightarrow> terminates prg (Suc pc) \<Longrightarrow> terminates prg pc"  
  | "prg pc = JMP pc' \<Longrightarrow> terminates prg pc' \<Longrightarrow> terminates prg pc"  
  
  lemma "big_step prg pc s s' \<Longrightarrow> terminates prg pc"
    apply (induction rule: big_step.induct)
      apply (auto intro: terminates.intros)  
      done
  
    
lemma "terminates prg pc \<Longrightarrow> \<exists>s'. big_step prg pc s s'" 
  apply (induction arbitrary: s rule: terminates.induct)
    apply (auto intro: big_step.intros)
    apply (force intro: big_step.intros)
    apply (force intro: big_step.intros)
    done    
    

  (* Finding Invariants *)  
context
  includes IMP_Syntax
begin  
  interpretation Vcg_Aux_Lemmas .   
    
(* The following function computes, for i\<ge>0, 
  2\<^sup>i * i!
  
  For negative i, it will always compute 1
*)
function fact2 :: "int \<Rightarrow> int" where
  "i>0 \<Longrightarrow> fact2 i = 2*i*fact2 (i-1)"
| "i\<le>0 \<Longrightarrow> fact2 i = 1"  
by auto fastforce
termination
  by (relation "measure nat") auto

(* Here is an IMP-Version of it.*)    
abbreviation "fact2_prog \<equiv> 
  CLR r;; CLR c;;
  r ::= N 1;;
  c ::= N 0;;
  WHILE ($c < $n) DO (
    c ::= $c + N 1;;
    r ::= \<acute>2 * $r * $c    
  )"
  
(* The VCG will generate the following VCs. Find an appropriate invariant and prove them!
  (medium..hard)
*)  
definition fact_invar :: "int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> bool" 
  where "fact_invar n r c \<equiv> r = fact2 c \<and> 0\<le>c \<and> c\<le>n"

  
lemma VC1: "\<lbrakk>c < z; fact_invar z r c\<rbrakk> \<Longrightarrow> fact_invar z (2 * r * (c + 1)) (c + 1)"  
  unfolding fact_invar_def by auto
  
lemma VC2: "\<lbrakk>\<not> c < z; fact_invar z r c\<rbrakk> \<Longrightarrow> r = fact2 z"
  unfolding fact_invar_def by auto
  
lemma VC3: "0 \<le> z \<Longrightarrow> fact_invar z 1 0"
  unfolding fact_invar_def by auto
  

lemma fact_prog_correct: " 
  \<Turnstile>\<^sub>t {\<lambda>n\<^sub>0. vars n in n=n\<^sub>0 \<and> 0\<le>n } 
       fact2_prog 
    {\<lambda>n\<^sub>0. vars n r in n=n\<^sub>0 \<and> r = (fact2 n\<^sub>0) } mod {''r'',''c''}"
  apply (rewrite annot_tinvar[where 
    I="\<lambda>n\<^sub>0. vars n r c in n=n\<^sub>0 \<and> fact_invar n\<^sub>0 r c"
  and R="measure_exp ($n-$c)"  
    ])
  apply (vcg_all; (rule VC1 VC2 VC3; assumption)?)
  (* Next, replace termination_exp by some meaningful measure expression,
    and conclude the proof! 
    
    (easy..medium)
    *)
  done  

end  
  
  (* Hoare-Logic *)  
  
  (* Which of the following Hoare triples hold? 
    Warning: Some may not be provable. Mark these by unprovable, 
    and give a short, intuitive explanation.
    
    (medium)
  *)
    
  lemma "\<Turnstile>\<^sub>t {\<lambda>z s. P1 z s \<or> P2 z s} c {Q} \<Longrightarrow> \<Turnstile>\<^sub>t {P1} c {Q} \<and> \<Turnstile>\<^sub>t {P2} c {Q}"  
    by (auto simp: hoaret_valid_def)
  
  lemma "\<Turnstile>\<^sub>t {P1} c {Q} \<and> \<Turnstile>\<^sub>t {P2} c {Q} \<Longrightarrow> \<Turnstile>\<^sub>t {\<lambda>z s. P1 z s \<or> P2 z s} c {Q}"  
    by (auto simp: hoaret_valid_def)
  
  lemma "\<Turnstile>\<^sub>t {\<lambda>z s. P1 z s \<and> P2 z s} c {Q} \<Longrightarrow> \<Turnstile>\<^sub>t {P1} c {Q} \<and> \<Turnstile>\<^sub>t {P2} c {Q}"  
    (* Might require conditions from P and Q to hold SIMULTANEOUSLY   *)
    oops
  
  lemma "\<Turnstile>\<^sub>t {P1} c {Q} \<and> \<Turnstile>\<^sub>t {P2} c {Q} \<Longrightarrow> \<Turnstile>\<^sub>t {\<lambda>z s. P1 z s \<and> P2 z s} c {Q}"  
    by (auto simp: hoaret_valid_def)
  
  
end
