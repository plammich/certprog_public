theory Tutorial7
imports Main
begin
  (*
    Note: While the difficulty of the questions in this tutorial may be encountered
      in an exam (among the more difficult questions), they will not depend on 
      each other so much. 
  *)
  
  section \<open>Balanced Words\<close>
  
  (*
    Lets consider an alphabet A = {L,R}. 
  *)
  
  datatype A = L | R
  type_synonym word = "A list"

  (* Define two functions, lcount and rcount, that count the 
     number Ls and Rs in a word.
     Define the functions by recursion over the list, and pattern matching!
     
    (Exam category: easy)
  *)

  fun lcount :: "A list \<Rightarrow> nat" where
    "lcount [] = 0" | "lcount (L#xs) = Suc (lcount xs)" | "lcount (R#xs) = lcount xs"
  
  fun rcount :: "A list \<Rightarrow> nat" where
    "rcount [] = 0" | "rcount (L#xs) = rcount xs" | "rcount (R#xs) = Suc (rcount xs)"

  (* A functional programmer might have written lcount as 
      length (filter (op = L) xs). 
    Prove that he is right doing so! 

    (Exam category: medium)
    *)  
  lemma "lcount xs = length (filter (op = L) xs)"
    by (induction xs rule: lcount.induct) auto
    
  (* Show that lcount xs is always less than or equal to the length of xs 
    (Exam category: medium)
  *)  
  lemma "lcount xs \<le> length xs"  
    by (induction xs rule: lcount.induct) auto
  
  (* Show that lcount xs + rcount xs equals the length of xs 
    (Exam category: medium)
  *)
  lemma "lcount xs + rcount xs = length xs"
    by (induction xs rule: lcount.induct) auto

  (* The following function also computes lcount! *)  
  fun lcount_aux :: "nat \<Rightarrow> A list \<Rightarrow> nat" where
    "lcount_aux a [] = a"
  | "lcount_aux a (L#xs) = lcount_aux (Suc a) xs"
  | "lcount_aux a (R#xs) = lcount_aux a xs"
    
  definition "lcount' xs = lcount_aux 0 xs"
  
  lemma lcount_aux_eq: "lcount_aux a xs = a + lcount xs"
    apply (induction arbitrary: a rule: lcount.induct)
    by auto
  
  (* Prove that! 
    (Exam category: medium..hard)
  *)  
  lemma "lcount' xs = lcount xs"
    unfolding lcount'_def by (simp add: lcount_aux_eq)
  
  (*
    A word w is balanced, if
      E: w = []
      S: w = L#w'@[R] and w' is balanced
      C: w = w\<^sub>1@w\<^sub>2 and both w\<^sub>1 and w\<^sub>2 are balanced
    
    Think of L as "(" and R as ")", then balanced words are exactly those with 
    no dangling parenthesis.
    
    
    Characterize balanced words by an inductive predicate!
    
    Name your introduction rules E, S, C. (mnemonic: Empty, Surround, Concatenate)
    
    (Exam category: medium)
  *)
  
  inductive balanced :: "word \<Rightarrow> bool" where
    E: "balanced []" 
  | S: "balanced w \<Longrightarrow> balanced (L#w@[R])"
  | C: "\<lbrakk>balanced w\<^sub>1; balanced w\<^sub>2\<rbrakk> \<Longrightarrow> balanced (w\<^sub>1@w\<^sub>2)"
  
  (* We can use forward theorem composition (OF) to construct
    balanced parentheses. For example:
  *)  
  (*               ( ( ) ( ) )   *)
  lemma "balanced [L,L,R,L,R,R]"
    using S[OF C[OF S[OF E] S[OF E]]]
    by simp  
    
  (* Use the same technique to prove: 
    (Exam category: medium)
  
                   ( ( ) ( ) ) ( ) ( )   *)    
  lemma "balanced [L,L,R,L,R,R,L,R,L,R]"
    (*<*)
    using C[OF C[OF S[OF C[OF S[OF E] S[OF E]]] S[OF E]] S[OF E]]
    by simp  
    (*>*)  
  
  lemma [simp]: "lcount (xs@[R]) = lcount xs"  
    by (induction xs rule: lcount.induct) auto
    
  lemma [simp]: "rcount (xs@[R]) = Suc (rcount xs)"  
    by (induction xs rule: rcount.induct) auto
    
  lemma [simp]: "rcount (xs@ys) = rcount xs + rcount ys" 
    by (induction xs rule: rcount.induct) auto
  
  lemma [simp]: "lcount (xs@ys) = lcount xs + lcount ys" 
    by (induction xs rule: rcount.induct) auto
    
  (* Show that balanced words have the same number of Ls and Rs.
    Hint: You'll need auxiliary lemmas, which are straightforward 
    to prove, though.
    
    (Exam category: hard)
    
  *)  
    
  lemma "balanced xs \<Longrightarrow> lcount xs = rcount xs"
    apply (induction rule: balanced.induct)
    apply auto
    done
    
  (* The following function flips parenthesis. *)  
  fun flip :: "A \<Rightarrow> A" where 
    "flip L = R" | "flip R = L"
    
  (* Show that a balanced word, read backwards and with parenthesis flipped, 
    is balanced again: 
    
    (Exam category: medium)
    *)
  lemma "balanced w \<Longrightarrow> balanced (rev (map flip w))"
    apply (induction rule: balanced.induct)
    apply (auto intro: balanced.intros)
    done
      
    
  section \<open>Tribonacci Numbers\<close>  

  (*
    The tribonacci number series starts with 0,0,1, and each next number
    is the sum of its 3 predecessors.
    
    Formalize this function in Isabelle!
    (Exam category: medium)
  *)  
  fun trib :: "nat \<Rightarrow> nat" where
    "trib 0 = 0"
  | "trib (Suc 0) = 0"
  | "trib (Suc (Suc 0)) = 1"
  | "trib (Suc (Suc (Suc n))) = trib n + trib (Suc n) + trib (Suc (Suc n))"
  
  
  (*
    Show monotonicity of trib.
    (Exam category: really hard)
  *)
  lemma "n\<le>m \<Longrightarrow> trib n \<le> trib m"
  proof (induction n arbitrary: m rule: trib.induct)
    case 1
    then show ?case by (induction m rule: trib.induct) auto
  next
    case 2
    then show ?case by (induction m rule: trib.induct) auto
  next
    case 3
    then show ?case by (induction m rule: trib.induct) auto
  next
    case (4 n)
    then show ?case 
      apply (cases m rule: trib.cases)
      by fastforce+
  qed
    
  
  section \<open>Eliminating ORs\<close>
  
  (* Consider Boolean formula over Boolean variables, and, or, not*)
  
  type_synonym vname = string
  datatype frml = BVar vname | And frml frml | Or frml frml | Not frml  
  
  (* Give them a semantics 
  
    (Exam category: medium)
  *)
  type_synonym state = "vname \<Rightarrow> bool" 

  fun fval :: "frml \<Rightarrow> state \<Rightarrow> bool" where
    "fval (BVar x) s \<longleftrightarrow> s x"
  | "fval (And a b) s \<longleftrightarrow> fval a s \<and> fval b s"  
  | "fval (Or a b) s \<longleftrightarrow> fval a s \<or> fval b s"  
  | "fval (Not a) s \<longleftrightarrow> \<not>fval a s"  

  (* Write a function that converts every formula into an equivalent formula
    that does not contain Or! 
    
    Hint: Use de-Morgan's law: "a \<or> b \<longleftrightarrow> \<not>(\<not>a \<and> \<not>b)"
    
    (Exam category: medium  (without the hint: hard))
  *)
  fun elim_or :: "frml \<Rightarrow> frml" where
    "elim_or (BVar x) = BVar x"
  | "elim_or (And a b) = And (elim_or a) (elim_or b)"  
  | "elim_or (Or a b) = Not (And (Not (elim_or a)) (Not (elim_or b)))"
  | "elim_or (Not a) = Not (elim_or a)"

  (* Show that the generated formula is actually equivalent.
    (Come up with the lemma yourself!)

    (Exam category: medium..hard)  (with given lemma: easy)
  *)
  lemma "fval (elim_or f) s = fval f s"
    by (induction f) auto

  (*
    Define a function to check whether a formula contains an Or
    (Exam category: easy..medium)
  *)  
  fun contains_or :: "frml \<Rightarrow> bool" where
    "contains_or (BVar x) = False"
  | "contains_or (And a b) \<longleftrightarrow> contains_or a \<or> contains_or b"
  | "contains_or (Or a b) \<longleftrightarrow> True"  
  | "contains_or (Not a) \<longleftrightarrow> contains_or a"  

  (* Show that your or-elimination actually eliminates the ors 
    (Exam category: easy..medium)
  *)
  lemma "\<not>contains_or (elim_or f)"  
    by (induction f) auto
  
  
    

end
