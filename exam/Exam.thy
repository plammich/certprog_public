theory Exam
imports IPP_Lib
begin
  (*******************
    FINAL EXAM: CERTIFIED PROGRAMMING.
  
    Theories will work in Isabelle2016-1, and might also work in Isabelle2017.
    
    Note, you can use the sidekick-panel to get a quick overview of the contents
    of this exam.
    
    UNDERGRADUATE SECTION: Solve all questions but the last one, 
      which is explicitly marked as GRADUATE ONLY!
      
      You have two hours to work on this exam. 
      Afterwards, you are supposed to upload your solution via the 
      canvas-system, or as will be announced in the classroom.
      

    GRADUATE SECTION: Solve all questions!  
    
      You have 24 hours to work on this exam.
      Afterwards, upload your solution in the canvas system.
      In case of technical problems, you may submit your solution via email to one
      of the following addresses:
        lpeter1@vt.edu
        lammich@in.tum.de  (use this one only if vt-email 
                            should be unexpectedly down, and there is no other 
                            way of submitting, as your solution
                            would be stored on a server outside VT)
                            
    
    
  *******************)


section \<open>Q1. If-Then-Else\<close>

  (* We define Boolean expressions that consists of 
    constants, variables, and if-then-else expressions 
  *)  
  type_synonym vname = string  
  datatype ite = 
      IBC bool          (* Constant: True or False*)
    | IBV vname         (* Boolean Variable *)
    | ITE ite ite ite   (* if-then-else, in C++ or Java, one would also write "b\<^sub>1?b\<^sub>2:b\<^sub>3" *)
  
  (* A state maps variables to Boolean values *)
  type_synonym state = "vname \<Rightarrow> bool"

  subsection \<open>a) semantics of ite (easy+)\<close>
  (* Specify the semantics of if-then-else expressions
    (easy..medium)
   *)
  fun ival :: "ite \<Rightarrow> state \<Rightarrow> bool" where "ival _ _ \<longleftrightarrow> undefined"
  
      
  subsection \<open>b) semantics of standard bexp (easy)\<close>
  (* We also define more standard Boolean expressions with conjunction and negation. *)  
  datatype bexp = BC bool | BV vname | AND bexp bexp | NOT bexp
  (* Specify the semantics of standard Boolean expressions
    (easy)
  *)
  fun bval :: "bexp \<Rightarrow> state \<Rightarrow> bool" where
    "bval _ _ \<longleftrightarrow> undefined"

  subsection \<open>c) translation (easy+)\<close>
  
  (*
    Translate a standard Boolean expression into an equivalent if-then-else expression,
    and show that your translation is correct
  
    (easy..medium)
  *)  
  fun xlate :: "bexp \<Rightarrow> ite" where
    "xlate _ = undefined"

  lemma "ival (xlate b) s = bval b s" sorry
  
      
    
section \<open>Q2: Simple Balanced Words\<close>    

  (*
    We consider 'simple' words, a restricted form of balanced words, namely those 
    of the form (\<dots>()\<dots>), or, equivalently L\<^sup>nR\<^sup>n.
  *)    
  datatype A = L | R
  type_synonym word = "A list"

  (*
    These are created by the Empty and Surround rule. There is no Concatenate rule.
  *)    
  inductive simple :: "word \<Rightarrow> bool" where
    E: "simple []"    
  | S: "simple w \<Longrightarrow> simple (L#w@[R])"

  subsection \<open>a) Even Length (easy)\<close>
  (*
    Show: Every simple word has even length
    
    (easy)
  *)    
  lemma "simple w \<Longrightarrow> even (length w)" sorry

  subsection \<open>b) dup function (easy)\<close>  
  (*
    Define a recursive function \<open>dup n x\<close> to create a list of n \<open>x\<close>s, i.e.,
    [x,x,x,\<dots>x] (n times)

    Do not use functions from the Isabelle library in your definition.
        
    (easy)
  *)  
  fun dup :: "nat \<Rightarrow> 'a \<Rightarrow> 'a list" where
    "dup _ _ = undefined"
    
  subsection \<open>c) Simple words have form \<open>L\<^sup>nR\<^sup>n\<close> (medium+)\<close>
  (*
    Show that every simple word can actually be expressed as "dup n L @ dup n R"
    
    Hint: You might want to use an Isar proof, to instantiate the \<exists> in a controlled way!
    Hint: You might want to prove an auxiliary lemma of the form
          \<open>dup n x @ [x] = \<dots>\<close> 
    
    NO SLEDGEHAMMER ALLOWED! I.e., you must not use metis or smt methods for this proof!
    
    (medium+)
  *)  
  lemma "simple w \<Longrightarrow> \<exists>n. w = dup n L @ dup n R" sorry
    
    
  
section \<open>Q3: Hoare-Rules without Precondition (medium)\<close>  
  

(*
  Recall the standard pattern for Hoare-triples, to use the logical variables
  to transfer the whole state from the precondition to the postcondition, such that
  the postcondition can refer to the old state before executing the command:
  
  \<Turnstile> {\<lambda>s\<^sub>0 s. s\<^sub>0=s \<and> P s\<^sub>0} c {\<lambda>s\<^sub>0 s. Q s\<^sub>0 s}  

  Being able to refer to the precondition's state in the postcondition,
  we are going to investigate whether we need a precondition at all.
  The idea is to use 
    "P s\<^sub>0 \<longrightarrow> Q s\<^sub>0 s"
  as postcondition.

  The following four statements justify this idea for partial (1,2) and total (3,4) correctness.
  Which of them do actually hold? Give a proof or a short intuitive explanation 
  why the statement does not hold.
  
  (medium)
*)

(********** Partial Correctness*)

lemma S1: "\<Turnstile> {\<lambda>s\<^sub>0 s. s\<^sub>0=s \<and> True} c {\<lambda>s\<^sub>0 s. P s\<^sub>0 \<longrightarrow> Q s\<^sub>0 s} \<Longrightarrow> \<Turnstile> {\<lambda>s\<^sub>0 s. s\<^sub>0=s \<and> P s\<^sub>0} c {Q}"
  oops 

lemma S2: "\<Turnstile> {\<lambda>s\<^sub>0 s. s\<^sub>0=s \<and> P s\<^sub>0} c {Q} \<Longrightarrow> \<Turnstile> {\<lambda>s\<^sub>0 s. s=s\<^sub>0 \<and> True} c {\<lambda>s\<^sub>0 s. P s\<^sub>0 \<longrightarrow> Q s\<^sub>0 s}"
  oops 

(********** Total Correctness*)
  
lemma S3: "\<Turnstile>\<^sub>t {\<lambda>s\<^sub>0 s. s\<^sub>0=s \<and> True} c {\<lambda>s\<^sub>0 s. P s\<^sub>0 \<longrightarrow> Q s\<^sub>0 s} \<Longrightarrow> \<Turnstile>\<^sub>t {\<lambda>s\<^sub>0 s. P s\<^sub>0 \<and> s\<^sub>0=s} c {Q}"
  oops 
  
lemma S4: "\<Turnstile>\<^sub>t {\<lambda>s\<^sub>0 s. s\<^sub>0=s \<and> P s\<^sub>0} c {Q} \<Longrightarrow> \<Turnstile>\<^sub>t {\<lambda>s\<^sub>0 s. s\<^sub>0=s \<and> True} c {\<lambda>s\<^sub>0 s. P s\<^sub>0 \<longrightarrow> Q s\<^sub>0 s}"
  oops 


section \<open>Q4: Finding Invariant and Variant\<close>

context
  includes IMP_Syntax
begin  
  interpretation Vcg_Aux_Lemmas .   

  (*
    The following program sums up the elements of the range l..<h in array a, 
    by iterating from h down to l.
    
    Note that this uses a fresh variable i for iteration, 
    and thus, unlike the count-down scheme from the lecture, 
    does not modify the initial value of l or h (Which makes things easier!).
    
  *)  
  definition "array_sum \<equiv> 
    CLR r;; CLR i;;
    r ::= N 0;;
    i ::= $h;;
    WHILE ($i > $l) DO (
      i ::= $i - \<acute>1;;
      r ::= $r + a\<^bold>[$i\<^bold>]
    )"
  
  subsection \<open>a) Invariant (medium)\<close>    
  (*
  
    We want to prove the following:

    \<Turnstile>\<^sub>t {\<lambda>_. vars (a:imap) l h in l\<le>h}
         array_sum 
      {\<lambda>_. vars (a:imap) l h r in r = sum a {l..<h} } mod {''r'',''i''}"
    
  
    Specify an invariant, and prove the verification conditions!
    
    Quickcheck should work. 
    
    Nitpick may actually 
    warn you about "potentially spurious" counterexamples \<dots> these 
    were actually spurious for me, so don't pay too much attention to those!
    

    (medium)
  *)  
    
  (*
    Note that "sum a {l..<h}", which can also be written as "\<Sum>j=l..<h. a j",
    sums up the elements of the array a in range l..<h, i.e., l INCLUSIVE and h EXCLUSIVE.
  *)  
  term "sum a {l..<h}" term "\<Sum>j=l..<h. a j"
    
  definition array_sum_invar :: "(int\<Rightarrow>int) \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> bool" 
    where "array_sum_invar a l h i r \<longleftrightarrow> undefined"
    
  (* Invariant preserved by loop body: {I \<and> b} x {I} *)  
  lemma VC1: "\<lbrakk>l < i; array_sum_invar a l h i r\<rbrakk> \<Longrightarrow> array_sum_invar a l h (i - 1) (r + a (i - 1))"
    (* Hint: the lemma intvs_decr_l might be useful in the simpset *)
    thm intvs_decr_l
    sorry
    
  (* Invariant implies postcondition: I \<and> \<not>b \<Longrightarrow> Q *)  
  lemma VC2: "\<lbrakk>\<not> l < i; array_sum_invar a l h i r\<rbrakk> \<Longrightarrow> r = sum a {l..<h}"
    sorry
    
  (* Invariant holds initially: P \<Longrightarrow> I *)  
  lemma VC3: "l \<le> h \<Longrightarrow> array_sum_invar a l h h 0"
    sorry
    
  lemma fact'_correct: " 
    \<Turnstile>\<^sub>t {\<lambda>_. vars (a:imap) l h in l\<le>h}
         array_sum 
      {\<lambda>_. vars (a:imap) l h r in r = sum a {l..<h} } mod {''r'',''i''}"
    unfolding array_sum_def  
    apply (rewrite annot_tinvar[where 
        R="measure_exp (termination_exp)"
      and I="\<lambda>_. vars (a:imap) l h i r in array_sum_invar a l h i r
    "])
    apply (vcg_all; (rule VC1 VC2 VC3; assumption)?)
    
  subsection \<open>b) Variant/Termination (easy)\<close>
    
    (* Replace termination_exp by a meaningful measure expression, and complete the proof.
    
      Note: You can also solve this if you did not manage to find a working invariant:
        For this loop pattern, there is a measure expression that works without
        a loop invariant, and will be proved automatically once you get it right!

      (easy)
    
    *)
    sorry
  
end


section \<open>Q5: Push/Pop\<close>


  (* The following is an abstraction of a stack machine.
    Only the height of the stack is considered, the operations 
    are push and pop.
  *)  
  datatype instr = PUSH | POP  

  (*
    The following function computes the new height after executing a list of instructions,
    or returns None on stack underflow.
  *)    
  fun height :: "instr list \<Rightarrow> nat \<Rightarrow> nat option" where
    "height [] n = Some n" 
  | "height (PUSH#xs) n = height xs (Suc n)" 
  | "height (POP#xs) (Suc n) = height xs n" 
  | "height _ _ = None"

  subsection \<open>a) Same-Level Sequences (medium+)\<close>
  (*
    A same-level sequence is an instruction sequence that can run from an empty stack, 
    and yields an empty stack again.
    
    Show that any same-level sequence, when started in height n, 
      will yield height n again.
    
    (medium+)
  *)
  lemma lift_sl: "height xs 0 = Some 0 \<Longrightarrow> height xs n = Some n" sorry
  
  subsection \<open>b) Elimination \<open>PUSH@sl#POP\<close>  (hard) \<close>
  (*
    Prove that eliminating a subsequence of the form \<open>PUSH sl POP\<close>,
    where sl is a same-level path, does not change the semantics of 
    the instruction sequence.
    
    Hints:
      Start your proof with a case split on successful execution and failing execution.
      You might want to develop some auxiliary lemmas about concatenating/splitting of executions.
        
    (hard)
  *)

  lemma "height xs2 0 = Some 0 
    \<Longrightarrow> height (xs1@PUSH#xs2@POP#xs3) m = res \<longleftrightarrow> height (xs1@xs3) m = res"
    sorry

  
    
section \<open>Q6 [GRADUATE ONLY] Variables of Arithmetic Expression\<close>
(*
  Only needs to be solved by students of the graduate section of this course.
*)
    
  subsection \<open>a) vars_of function (easy+)\<close>
  (*
    Define a function that returns the set of variables 
    occurring in an arithmetic expression.
    Note: We use the arithmetic expressions from the project's extended 
      IMP language with arrays, CTRL+Click on the aexp type to jump to their definition!
    
    (easy+)
  *)
  fun vars_of :: "aexp \<Rightarrow> vname set" where
    "vars_of _ = undefined"
    
  subsection \<open>b) Independence of unused variables (medium)\<close>
  (*
    Show that the value of an expression does not depend on a variable not 
    occurring in the expression. Formalize the statement yourself!
    (medium)
  *)
  lemma "undefined" sorry

  subsection \<open>c) Renaming (medium)\<close>
  (*
    Define a function that renames all variables in an expression
    according to a renaming function \<sigma>: vname\<Rightarrow>vname

    Then show that renaming the variables in the expression 
    has the same effect as renaming the variables in the state.
        
    (medium)
  *)    
  fun subst :: "(vname \<Rightarrow> vname) \<Rightarrow> aexp \<Rightarrow> aexp" where
    "subst _ _ = undefined"

  lemma "aval (subst \<sigma> a) s = aval a (s o \<sigma>)"
    sorry
    
    
end
